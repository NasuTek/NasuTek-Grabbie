﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie {
    public partial class AdvancedSettings : Form {
        public AdvancedSettings() {
            InitializeComponent();
        }

        private void SettingsDialog_Load(object sender, EventArgs e) {
            foreach (var settingsPage in Program.Engine.SettingsPages) {
                var node = new TreeNode(settingsPage.PageName) {
                    Tag = settingsPage
                };

                settingsPage.Open();
                treeView1.Nodes.Add(node);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
            panel1.Controls.Clear();
            panel1.Controls.Add((Control) e.Node.Tag);
        }

        private void SettingsDialog_FormClosing(object sender, FormClosingEventArgs e) {
            panel1.Controls.Clear();
            treeView1.Nodes.Clear();
        }

        private void button2_Click(object sender, EventArgs e) {
            Close();
        }

        private void button1_Click(object sender, EventArgs e) {
            foreach (var node in treeView1.Nodes.Cast<TreeNode>().Where(node => node.Tag as SettingsPage != null))
                ((SettingsPage) node.Tag).Save();

            Close();
        }
    }
}