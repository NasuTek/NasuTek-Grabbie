﻿namespace NasuTek.Grabbie
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.panelBottom = new System.Windows.Forms.Panel();
            this.pbBottomPanelLine = new System.Windows.Forms.PictureBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panelSideBar = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblSeeAlso = new System.Windows.Forms.Label();
            this.lnkAddons = new System.Windows.Forms.LinkLabel();
            this.lblTasks = new System.Windows.Forms.Label();
            this.lnkKeyboardSettings = new System.Windows.Forms.LinkLabel();
            this.lnkUploadSettings = new System.Windows.Forms.LinkLabel();
            this.lnkAboutGrabbie = new System.Windows.Forms.LinkLabel();
            this.lnkBasicSettings = new System.Windows.Forms.LinkLabel();
            this.cmdInstallAddon = new System.Windows.Forms.ToolStripButton();
            this.cmdUninstallAddon = new System.Windows.Forms.ToolStripButton();
            this.cmdEnableDisableAddon = new System.Windows.Forms.ToolStripButton();
            this.cmdChangeAddonSettings = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new NasuTek.Grabbie.CustomTabControl();
            this.mpBasicSettings = new System.Windows.Forms.TabPage();
            this.lblDescBasicSettings = new System.Windows.Forms.Label();
            this.lblSectionBasicSettingsProgressNotifications = new System.Windows.Forms.Label();
            this.ckbxStartOnLogon = new System.Windows.Forms.CheckBox();
            this.lblProgressStyle = new System.Windows.Forms.Label();
            this.cbStatusType = new System.Windows.Forms.ComboBox();
            this.ckbxCloseProgress = new System.Windows.Forms.CheckBox();
            this.ckbxShowPreview = new System.Windows.Forms.CheckBox();
            this.lblHeadingBasicSettings = new System.Windows.Forms.Label();
            this.lblSectionBasicSettingsStartupPreview = new System.Windows.Forms.Label();
            this.pbSectionBasicSettingsProgressNotifications = new System.Windows.Forms.PictureBox();
            this.pbSectionBasicSettingsStartupPreview = new System.Windows.Forms.PictureBox();
            this.mpUploadSettings = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.cb = new System.Windows.Forms.ComboBox();
            this.lblUrlShortner = new System.Windows.Forms.Label();
            this.lblUploadSettingsUrlShortner = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbUploadMethod = new System.Windows.Forms.ComboBox();
            this.lblUploadMethod = new System.Windows.Forms.Label();
            this.lblUploadSettingsLocMethod = new System.Windows.Forms.Label();
            this.rbJPG = new System.Windows.Forms.RadioButton();
            this.rbPNG = new System.Windows.Forms.RadioButton();
            this.lblImageFileFormat = new System.Windows.Forms.Label();
            this.cbFileNameScheme = new System.Windows.Forms.ComboBox();
            this.lblDescUploadSettings = new System.Windows.Forms.Label();
            this.lblHeadingUploadSettings = new System.Windows.Forms.Label();
            this.lblFileNameScheme = new System.Windows.Forms.Label();
            this.lblUploadSettingsFileNameFormat = new System.Windows.Forms.Label();
            this.pbUploadSettingsLocMethod = new System.Windows.Forms.PictureBox();
            this.pbUploadSettingsFileNameFormat = new System.Windows.Forms.PictureBox();
            this.mpKeyboardShortcuts = new System.Windows.Forms.TabPage();
            this.panelClipboardAddon = new System.Windows.Forms.Panel();
            this.pbExampleClipboard = new System.Windows.Forms.PictureBox();
            this.lblClipboardShortcutKey = new System.Windows.Forms.Label();
            this.lblClipboard = new System.Windows.Forms.Label();
            this.tbClipboardShortcutKey = new System.Windows.Forms.TextBox();
            this.lblDescKeyboardShortcuts = new System.Windows.Forms.Label();
            this.lblSelectionShortcutKey = new System.Windows.Forms.Label();
            this.lblActiveWindowShortcutKey = new System.Windows.Forms.Label();
            this.lblWholeDesktopShortcutKey = new System.Windows.Forms.Label();
            this.tbSelectionShortcutKey = new System.Windows.Forms.TextBox();
            this.tbActiveWindowShortcutKey = new System.Windows.Forms.TextBox();
            this.tbWholeDesktopShortcutKey = new System.Windows.Forms.TextBox();
            this.lblSelection = new System.Windows.Forms.Label();
            this.lblActiveWindow = new System.Windows.Forms.Label();
            this.lblWholeDesktop = new System.Windows.Forms.Label();
            this.lblHeadingKeyboardShortcuts = new System.Windows.Forms.Label();
            this.pbExampleSelection = new System.Windows.Forms.PictureBox();
            this.pbExampleActiveWindow = new System.Windows.Forms.PictureBox();
            this.pbExampleWholeDesktop = new System.Windows.Forms.PictureBox();
            this.mpAddons = new System.Windows.Forms.TabPage();
            this.lvAddons = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblDescAddons = new System.Windows.Forms.Label();
            this.lblHeadingAddons = new System.Windows.Forms.Label();
            this.tsAddonsCommandBar = new System.Windows.Forms.ToolStrip();
            this.mpAboutGrabbie = new System.Windows.Forms.TabPage();
            this.lblHeadingAbout = new System.Windows.Forms.Label();
            this.lnkWebsite = new System.Windows.Forms.LinkLabel();
            this.mltbLicence = new System.Windows.Forms.TextBox();
            this.pbLicence = new System.Windows.Forms.PictureBox();
            this.lblLicence = new System.Windows.Forms.Label();
            this.lblBuildTag = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.pbVersion = new System.Windows.Forms.PictureBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pbProductLogo = new System.Windows.Forms.PictureBox();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBottomPanelLine)).BeginInit();
            this.panelSideBar.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.mpBasicSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSectionBasicSettingsProgressNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSectionBasicSettingsStartupPreview)).BeginInit();
            this.mpUploadSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUploadSettingsLocMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUploadSettingsFileNameFormat)).BeginInit();
            this.mpKeyboardShortcuts.SuspendLayout();
            this.panelClipboardAddon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleClipboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleActiveWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleWholeDesktop)).BeginInit();
            this.mpAddons.SuspendLayout();
            this.mpAboutGrabbie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLicence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbProductLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBottom
            // 
            this.panelBottom.BackColor = System.Drawing.SystemColors.Control;
            this.panelBottom.Controls.Add(this.pbBottomPanelLine);
            this.panelBottom.Controls.Add(this.btnSave);
            this.panelBottom.Controls.Add(this.btnCancel);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(200, 440);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(514, 46);
            this.panelBottom.TabIndex = 2;
            // 
            // pbBottomPanelLine
            // 
            this.pbBottomPanelLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbBottomPanelLine.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbBottomPanelLine.Location = new System.Drawing.Point(0, 0);
            this.pbBottomPanelLine.Name = "pbBottomPanelLine";
            this.pbBottomPanelLine.Size = new System.Drawing.Size(514, 1);
            this.pbBottomPanelLine.TabIndex = 2;
            this.pbBottomPanelLine.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(322, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(427, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panelSideBar
            // 
            this.panelSideBar.BackColor = System.Drawing.Color.Transparent;
            this.panelSideBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelSideBar.BackgroundImage")));
            this.panelSideBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelSideBar.Controls.Add(this.linkLabel1);
            this.panelSideBar.Controls.Add(this.lblSeeAlso);
            this.panelSideBar.Controls.Add(this.lnkAddons);
            this.panelSideBar.Controls.Add(this.lblTasks);
            this.panelSideBar.Controls.Add(this.lnkKeyboardSettings);
            this.panelSideBar.Controls.Add(this.lnkUploadSettings);
            this.panelSideBar.Controls.Add(this.lnkAboutGrabbie);
            this.panelSideBar.Controls.Add(this.lnkBasicSettings);
            this.panelSideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideBar.Location = new System.Drawing.Point(0, 0);
            this.panelSideBar.Name = "panelSideBar";
            this.panelSideBar.Size = new System.Drawing.Size(200, 486);
            this.panelSideBar.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(35, 102);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(85, 13);
            this.linkLabel1.TabIndex = 15;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Plugin Settings";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.White;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lblSeeAlso
            // 
            this.lblSeeAlso.AutoSize = true;
            this.lblSeeAlso.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeeAlso.ForeColor = System.Drawing.Color.White;
            this.lblSeeAlso.Location = new System.Drawing.Point(35, 420);
            this.lblSeeAlso.Name = "lblSeeAlso";
            this.lblSeeAlso.Size = new System.Drawing.Size(49, 13);
            this.lblSeeAlso.TabIndex = 12;
            this.lblSeeAlso.Text = "See also";
            // 
            // lnkAddons
            // 
            this.lnkAddons.ActiveLinkColor = System.Drawing.Color.White;
            this.lnkAddons.AutoSize = true;
            this.lnkAddons.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkAddons.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkAddons.LinkColor = System.Drawing.Color.White;
            this.lnkAddons.Location = new System.Drawing.Point(35, 442);
            this.lnkAddons.Name = "lnkAddons";
            this.lnkAddons.Size = new System.Drawing.Size(47, 13);
            this.lnkAddons.TabIndex = 14;
            this.lnkAddons.TabStop = true;
            this.lnkAddons.Text = "Addons";
            this.lnkAddons.VisitedLinkColor = System.Drawing.Color.White;
            this.lnkAddons.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAddons_LinkClicked);
            // 
            // lblTasks
            // 
            this.lblTasks.AutoSize = true;
            this.lblTasks.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTasks.ForeColor = System.Drawing.Color.White;
            this.lblTasks.Location = new System.Drawing.Point(35, 14);
            this.lblTasks.Name = "lblTasks";
            this.lblTasks.Size = new System.Drawing.Size(35, 13);
            this.lblTasks.TabIndex = 0;
            this.lblTasks.Text = "Tasks";
            // 
            // lnkKeyboardSettings
            // 
            this.lnkKeyboardSettings.ActiveLinkColor = System.Drawing.Color.White;
            this.lnkKeyboardSettings.AutoSize = true;
            this.lnkKeyboardSettings.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkKeyboardSettings.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkKeyboardSettings.LinkColor = System.Drawing.Color.White;
            this.lnkKeyboardSettings.Location = new System.Drawing.Point(35, 80);
            this.lnkKeyboardSettings.Name = "lnkKeyboardSettings";
            this.lnkKeyboardSettings.Size = new System.Drawing.Size(106, 13);
            this.lnkKeyboardSettings.TabIndex = 11;
            this.lnkKeyboardSettings.TabStop = true;
            this.lnkKeyboardSettings.Text = "Keyboard shortcuts";
            this.lnkKeyboardSettings.VisitedLinkColor = System.Drawing.Color.White;
            this.lnkKeyboardSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkKeyboardSettings_LinkClicked);
            // 
            // lnkUploadSettings
            // 
            this.lnkUploadSettings.ActiveLinkColor = System.Drawing.Color.White;
            this.lnkUploadSettings.AutoSize = true;
            this.lnkUploadSettings.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkUploadSettings.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkUploadSettings.LinkColor = System.Drawing.Color.White;
            this.lnkUploadSettings.Location = new System.Drawing.Point(35, 58);
            this.lnkUploadSettings.Name = "lnkUploadSettings";
            this.lnkUploadSettings.Size = new System.Drawing.Size(89, 13);
            this.lnkUploadSettings.TabIndex = 9;
            this.lnkUploadSettings.TabStop = true;
            this.lnkUploadSettings.Text = "Upload settings";
            this.lnkUploadSettings.VisitedLinkColor = System.Drawing.Color.White;
            this.lnkUploadSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkUploadSettings_LinkClicked);
            // 
            // lnkAboutGrabbie
            // 
            this.lnkAboutGrabbie.ActiveLinkColor = System.Drawing.Color.White;
            this.lnkAboutGrabbie.AutoSize = true;
            this.lnkAboutGrabbie.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkAboutGrabbie.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkAboutGrabbie.LinkColor = System.Drawing.Color.White;
            this.lnkAboutGrabbie.Location = new System.Drawing.Point(35, 464);
            this.lnkAboutGrabbie.Name = "lnkAboutGrabbie";
            this.lnkAboutGrabbie.Size = new System.Drawing.Size(83, 13);
            this.lnkAboutGrabbie.TabIndex = 7;
            this.lnkAboutGrabbie.TabStop = true;
            this.lnkAboutGrabbie.Text = "About Grabbie";
            this.lnkAboutGrabbie.VisitedLinkColor = System.Drawing.Color.White;
            this.lnkAboutGrabbie.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAboutGrabbie_LinkClicked);
            // 
            // lnkBasicSettings
            // 
            this.lnkBasicSettings.ActiveLinkColor = System.Drawing.Color.White;
            this.lnkBasicSettings.AutoSize = true;
            this.lnkBasicSettings.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkBasicSettings.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkBasicSettings.LinkColor = System.Drawing.Color.White;
            this.lnkBasicSettings.Location = new System.Drawing.Point(35, 36);
            this.lnkBasicSettings.Name = "lnkBasicSettings";
            this.lnkBasicSettings.Size = new System.Drawing.Size(77, 13);
            this.lnkBasicSettings.TabIndex = 1;
            this.lnkBasicSettings.TabStop = true;
            this.lnkBasicSettings.Text = "Basic settings";
            this.lnkBasicSettings.VisitedLinkColor = System.Drawing.Color.White;
            this.lnkBasicSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkBasicSettings_LinkClicked);
            // 
            // cmdInstallAddon
            // 
            this.cmdInstallAddon.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.cmdInstallAddon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdInstallAddon.Image = ((System.Drawing.Image)(resources.GetObject("cmdInstallAddon.Image")));
            this.cmdInstallAddon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdInstallAddon.Name = "cmdInstallAddon";
            this.cmdInstallAddon.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cmdInstallAddon.Size = new System.Drawing.Size(105, 30);
            this.cmdInstallAddon.Text = "Install an addon";
            this.cmdInstallAddon.ToolTipText = "Adds a new addon to extend Grabbie\'s functionality.";
            this.cmdInstallAddon.Click += new System.EventHandler(this.cmdInstallAddon_Click);
            // 
            // cmdUninstallAddon
            // 
            this.cmdUninstallAddon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdUninstallAddon.Image = ((System.Drawing.Image)(resources.GetObject("cmdUninstallAddon.Image")));
            this.cmdUninstallAddon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdUninstallAddon.Name = "cmdUninstallAddon";
            this.cmdUninstallAddon.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cmdUninstallAddon.Size = new System.Drawing.Size(67, 30);
            this.cmdUninstallAddon.Text = "Uninstall";
            this.cmdUninstallAddon.ToolTipText = "Removes this addon.";
            // 
            // cmdEnableDisableAddon
            // 
            this.cmdEnableDisableAddon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdEnableDisableAddon.Image = ((System.Drawing.Image)(resources.GetObject("cmdEnableDisableAddon.Image")));
            this.cmdEnableDisableAddon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdEnableDisableAddon.Name = "cmdEnableDisableAddon";
            this.cmdEnableDisableAddon.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cmdEnableDisableAddon.Size = new System.Drawing.Size(59, 30);
            this.cmdEnableDisableAddon.Text = "Disable";
            this.cmdEnableDisableAddon.ToolTipText = "Disables this addon.";
            // 
            // cmdChangeAddonSettings
            // 
            this.cmdChangeAddonSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdChangeAddonSettings.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangeAddonSettings.Image")));
            this.cmdChangeAddonSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdChangeAddonSettings.Name = "cmdChangeAddonSettings";
            this.cmdChangeAddonSettings.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cmdChangeAddonSettings.Size = new System.Drawing.Size(62, 30);
            this.cmdChangeAddonSettings.Text = "Change";
            this.cmdChangeAddonSettings.ToolTipText = "Change this addon\'s settings.";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.mpBasicSettings);
            this.tabControl1.Controls.Add(this.mpUploadSettings);
            this.tabControl1.Controls.Add(this.mpKeyboardShortcuts);
            this.tabControl1.Controls.Add(this.mpAddons);
            this.tabControl1.Controls.Add(this.mpAboutGrabbie);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(200, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(514, 440);
            this.tabControl1.TabIndex = 3;
            // 
            // mpBasicSettings
            // 
            this.mpBasicSettings.BackColor = System.Drawing.Color.White;
            this.mpBasicSettings.Controls.Add(this.lblDescBasicSettings);
            this.mpBasicSettings.Controls.Add(this.lblSectionBasicSettingsProgressNotifications);
            this.mpBasicSettings.Controls.Add(this.ckbxStartOnLogon);
            this.mpBasicSettings.Controls.Add(this.lblProgressStyle);
            this.mpBasicSettings.Controls.Add(this.cbStatusType);
            this.mpBasicSettings.Controls.Add(this.ckbxCloseProgress);
            this.mpBasicSettings.Controls.Add(this.ckbxShowPreview);
            this.mpBasicSettings.Controls.Add(this.lblHeadingBasicSettings);
            this.mpBasicSettings.Controls.Add(this.lblSectionBasicSettingsStartupPreview);
            this.mpBasicSettings.Controls.Add(this.pbSectionBasicSettingsProgressNotifications);
            this.mpBasicSettings.Controls.Add(this.pbSectionBasicSettingsStartupPreview);
            this.mpBasicSettings.Location = new System.Drawing.Point(4, 22);
            this.mpBasicSettings.Name = "mpBasicSettings";
            this.mpBasicSettings.Size = new System.Drawing.Size(506, 414);
            this.mpBasicSettings.TabIndex = 0;
            this.mpBasicSettings.Text = "managedPanel1";
            // 
            // lblDescBasicSettings
            // 
            this.lblDescBasicSettings.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescBasicSettings.Location = new System.Drawing.Point(18, 42);
            this.lblDescBasicSettings.Name = "lblDescBasicSettings";
            this.lblDescBasicSettings.Size = new System.Drawing.Size(484, 16);
            this.lblDescBasicSettings.TabIndex = 24;
            this.lblDescBasicSettings.Text = "These settings will allow you to change the basic settings for Grabbie.";
            // 
            // lblSectionBasicSettingsProgressNotifications
            // 
            this.lblSectionBasicSettingsProgressNotifications.AutoSize = true;
            this.lblSectionBasicSettingsProgressNotifications.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSectionBasicSettingsProgressNotifications.Location = new System.Drawing.Point(18, 153);
            this.lblSectionBasicSettingsProgressNotifications.Name = "lblSectionBasicSettingsProgressNotifications";
            this.lblSectionBasicSettingsProgressNotifications.Size = new System.Drawing.Size(137, 13);
            this.lblSectionBasicSettingsProgressNotifications.TabIndex = 22;
            this.lblSectionBasicSettingsProgressNotifications.Text = "Progress and notification";
            // 
            // ckbxStartOnLogon
            // 
            this.ckbxStartOnLogon.AutoSize = true;
            this.ckbxStartOnLogon.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbxStartOnLogon.Location = new System.Drawing.Point(72, 93);
            this.ckbxStartOnLogon.Name = "ckbxStartOnLogon";
            this.ckbxStartOnLogon.Size = new System.Drawing.Size(166, 17);
            this.ckbxStartOnLogon.TabIndex = 20;
            this.ckbxStartOnLogon.Text = "Start Grabbie when I logon";
            this.ckbxStartOnLogon.UseVisualStyleBackColor = true;
            // 
            // lblProgressStyle
            // 
            this.lblProgressStyle.AutoSize = true;
            this.lblProgressStyle.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressStyle.Location = new System.Drawing.Point(53, 180);
            this.lblProgressStyle.Name = "lblProgressStyle";
            this.lblProgressStyle.Size = new System.Drawing.Size(97, 13);
            this.lblProgressStyle.TabIndex = 19;
            this.lblProgressStyle.Text = "Notification style:";
            // 
            // cbStatusType
            // 
            this.cbStatusType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatusType.FormattingEnabled = true;
            this.cbStatusType.Location = new System.Drawing.Point(156, 177);
            this.cbStatusType.Name = "cbStatusType";
            this.cbStatusType.Size = new System.Drawing.Size(272, 21);
            this.cbStatusType.TabIndex = 18;
            this.cbStatusType.SelectedIndexChanged += new System.EventHandler(this.cbStatusType_SelectedIndexChanged);
            // 
            // ckbxCloseProgress
            // 
            this.ckbxCloseProgress.AutoSize = true;
            this.ckbxCloseProgress.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbxCloseProgress.Location = new System.Drawing.Point(73, 209);
            this.ckbxCloseProgress.Name = "ckbxCloseProgress";
            this.ckbxCloseProgress.Size = new System.Drawing.Size(305, 17);
            this.ckbxCloseProgress.TabIndex = 17;
            this.ckbxCloseProgress.Text = "Automaticly close the progress window when finished";
            this.ckbxCloseProgress.UseVisualStyleBackColor = true;
            // 
            // ckbxShowPreview
            // 
            this.ckbxShowPreview.AutoSize = true;
            this.ckbxShowPreview.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbxShowPreview.Location = new System.Drawing.Point(72, 121);
            this.ckbxShowPreview.Name = "ckbxShowPreview";
            this.ckbxShowPreview.Size = new System.Drawing.Size(255, 17);
            this.ckbxShowPreview.TabIndex = 16;
            this.ckbxShowPreview.Text = "Show a preview when I capture a screenshot";
            this.ckbxShowPreview.UseVisualStyleBackColor = true;
            // 
            // lblHeadingBasicSettings
            // 
            this.lblHeadingBasicSettings.AutoSize = true;
            this.lblHeadingBasicSettings.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingBasicSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.lblHeadingBasicSettings.Location = new System.Drawing.Point(17, 14);
            this.lblHeadingBasicSettings.Name = "lblHeadingBasicSettings";
            this.lblHeadingBasicSettings.Size = new System.Drawing.Size(225, 21);
            this.lblHeadingBasicSettings.TabIndex = 15;
            this.lblHeadingBasicSettings.Text = "Change basic program settings";
            // 
            // lblSectionBasicSettingsStartupPreview
            // 
            this.lblSectionBasicSettingsStartupPreview.AutoSize = true;
            this.lblSectionBasicSettingsStartupPreview.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSectionBasicSettingsStartupPreview.Location = new System.Drawing.Point(18, 69);
            this.lblSectionBasicSettingsStartupPreview.Name = "lblSectionBasicSettingsStartupPreview";
            this.lblSectionBasicSettingsStartupPreview.Size = new System.Drawing.Size(111, 13);
            this.lblSectionBasicSettingsStartupPreview.TabIndex = 12;
            this.lblSectionBasicSettingsStartupPreview.Text = "Startup and preview";
            // 
            // pbSectionBasicSettingsProgressNotifications
            // 
            this.pbSectionBasicSettingsProgressNotifications.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbSectionBasicSettingsProgressNotifications.Location = new System.Drawing.Point(157, 160);
            this.pbSectionBasicSettingsProgressNotifications.Name = "pbSectionBasicSettingsProgressNotifications";
            this.pbSectionBasicSettingsProgressNotifications.Size = new System.Drawing.Size(349, 1);
            this.pbSectionBasicSettingsProgressNotifications.TabIndex = 23;
            this.pbSectionBasicSettingsProgressNotifications.TabStop = false;
            // 
            // pbSectionBasicSettingsStartupPreview
            // 
            this.pbSectionBasicSettingsStartupPreview.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbSectionBasicSettingsStartupPreview.Location = new System.Drawing.Point(132, 76);
            this.pbSectionBasicSettingsStartupPreview.Name = "pbSectionBasicSettingsStartupPreview";
            this.pbSectionBasicSettingsStartupPreview.Size = new System.Drawing.Size(374, 1);
            this.pbSectionBasicSettingsStartupPreview.TabIndex = 13;
            this.pbSectionBasicSettingsStartupPreview.TabStop = false;
            // 
            // mpUploadSettings
            // 
            this.mpUploadSettings.BackColor = System.Drawing.Color.White;
            this.mpUploadSettings.Controls.Add(this.label1);
            this.mpUploadSettings.Controls.Add(this.cb);
            this.mpUploadSettings.Controls.Add(this.lblUrlShortner);
            this.mpUploadSettings.Controls.Add(this.lblUploadSettingsUrlShortner);
            this.mpUploadSettings.Controls.Add(this.pictureBox1);
            this.mpUploadSettings.Controls.Add(this.cbUploadMethod);
            this.mpUploadSettings.Controls.Add(this.lblUploadMethod);
            this.mpUploadSettings.Controls.Add(this.lblUploadSettingsLocMethod);
            this.mpUploadSettings.Controls.Add(this.rbJPG);
            this.mpUploadSettings.Controls.Add(this.rbPNG);
            this.mpUploadSettings.Controls.Add(this.lblImageFileFormat);
            this.mpUploadSettings.Controls.Add(this.cbFileNameScheme);
            this.mpUploadSettings.Controls.Add(this.lblDescUploadSettings);
            this.mpUploadSettings.Controls.Add(this.lblHeadingUploadSettings);
            this.mpUploadSettings.Controls.Add(this.lblFileNameScheme);
            this.mpUploadSettings.Controls.Add(this.lblUploadSettingsFileNameFormat);
            this.mpUploadSettings.Controls.Add(this.pbUploadSettingsLocMethod);
            this.mpUploadSettings.Controls.Add(this.pbUploadSettingsFileNameFormat);
            this.mpUploadSettings.Location = new System.Drawing.Point(4, 22);
            this.mpUploadSettings.Name = "mpUploadSettings";
            this.mpUploadSettings.Size = new System.Drawing.Size(506, 414);
            this.mpUploadSettings.TabIndex = 0;
            this.mpUploadSettings.Text = "managedPanel1";
            this.mpUploadSettings.Click += new System.EventHandler(this.mpUploadSettings_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(69, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(429, 21);
            this.label1.TabIndex = 33;
            this.label1.Text = "To change upload options, see Plugin Settings.";
            // 
            // cb
            // 
            this.cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb.FormattingEnabled = true;
            this.cb.Location = new System.Drawing.Point(175, 322);
            this.cb.Name = "cb";
            this.cb.Size = new System.Drawing.Size(255, 21);
            this.cb.TabIndex = 32;
            // 
            // lblUrlShortner
            // 
            this.lblUrlShortner.AutoSize = true;
            this.lblUrlShortner.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrlShortner.Location = new System.Drawing.Point(82, 325);
            this.lblUrlShortner.Name = "lblUrlShortner";
            this.lblUrlShortner.Size = new System.Drawing.Size(78, 13);
            this.lblUrlShortner.TabIndex = 31;
            this.lblUrlShortner.Text = "URL Shortner:";
            // 
            // lblUploadSettingsUrlShortner
            // 
            this.lblUploadSettingsUrlShortner.AutoSize = true;
            this.lblUploadSettingsUrlShortner.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUploadSettingsUrlShortner.Location = new System.Drawing.Point(18, 302);
            this.lblUploadSettingsUrlShortner.Name = "lblUploadSettingsUrlShortner";
            this.lblUploadSettingsUrlShortner.Size = new System.Drawing.Size(75, 13);
            this.lblUploadSettingsUrlShortner.TabIndex = 29;
            this.lblUploadSettingsUrlShortner.Text = "URL Shortner";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pictureBox1.Location = new System.Drawing.Point(94, 309);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(412, 1);
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // cbUploadMethod
            // 
            this.cbUploadMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUploadMethod.FormattingEnabled = true;
            this.cbUploadMethod.Location = new System.Drawing.Point(175, 248);
            this.cbUploadMethod.Name = "cbUploadMethod";
            this.cbUploadMethod.Size = new System.Drawing.Size(255, 21);
            this.cbUploadMethod.TabIndex = 28;
            // 
            // lblUploadMethod
            // 
            this.lblUploadMethod.AutoSize = true;
            this.lblUploadMethod.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUploadMethod.Location = new System.Drawing.Point(69, 251);
            this.lblUploadMethod.Name = "lblUploadMethod";
            this.lblUploadMethod.Size = new System.Drawing.Size(91, 13);
            this.lblUploadMethod.TabIndex = 27;
            this.lblUploadMethod.Text = "Upload method:";
            // 
            // lblUploadSettingsLocMethod
            // 
            this.lblUploadSettingsLocMethod.AutoSize = true;
            this.lblUploadSettingsLocMethod.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUploadSettingsLocMethod.Location = new System.Drawing.Point(18, 228);
            this.lblUploadSettingsLocMethod.Name = "lblUploadSettingsLocMethod";
            this.lblUploadSettingsLocMethod.Size = new System.Drawing.Size(156, 13);
            this.lblUploadSettingsLocMethod.TabIndex = 25;
            this.lblUploadSettingsLocMethod.Text = "Upload location and method";
            // 
            // rbJPG
            // 
            this.rbJPG.AutoSize = true;
            this.rbJPG.Location = new System.Drawing.Point(94, 198);
            this.rbJPG.Name = "rbJPG";
            this.rbJPG.Size = new System.Drawing.Size(114, 17);
            this.rbJPG.TabIndex = 24;
            this.rbJPG.TabStop = true;
            this.rbJPG.Text = "JPG (Smallest size)";
            this.rbJPG.UseVisualStyleBackColor = true;
            // 
            // rbPNG
            // 
            this.rbPNG.AccessibleDescription = "rbJPG";
            this.rbPNG.AutoSize = true;
            this.rbPNG.Location = new System.Drawing.Point(94, 175);
            this.rbPNG.Name = "rbPNG";
            this.rbPNG.Size = new System.Drawing.Size(111, 17);
            this.rbPNG.TabIndex = 23;
            this.rbPNG.TabStop = true;
            this.rbPNG.Text = "PNG (Best quality)";
            this.rbPNG.UseVisualStyleBackColor = true;
            // 
            // lblImageFileFormat
            // 
            this.lblImageFileFormat.AutoSize = true;
            this.lblImageFileFormat.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImageFileFormat.Location = new System.Drawing.Point(69, 151);
            this.lblImageFileFormat.Name = "lblImageFileFormat";
            this.lblImageFileFormat.Size = new System.Drawing.Size(97, 13);
            this.lblImageFileFormat.TabIndex = 22;
            this.lblImageFileFormat.Text = "Image file format:";
            // 
            // cbFileNameScheme
            // 
            this.cbFileNameScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFileNameScheme.FormattingEnabled = true;
            this.cbFileNameScheme.Location = new System.Drawing.Point(175, 116);
            this.cbFileNameScheme.Name = "cbFileNameScheme";
            this.cbFileNameScheme.Size = new System.Drawing.Size(255, 21);
            this.cbFileNameScheme.TabIndex = 21;
            // 
            // lblDescUploadSettings
            // 
            this.lblDescUploadSettings.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescUploadSettings.Location = new System.Drawing.Point(18, 42);
            this.lblDescUploadSettings.Name = "lblDescUploadSettings";
            this.lblDescUploadSettings.Size = new System.Drawing.Size(484, 44);
            this.lblDescUploadSettings.TabIndex = 20;
            this.lblDescUploadSettings.Text = resources.GetString("lblDescUploadSettings.Text");
            // 
            // lblHeadingUploadSettings
            // 
            this.lblHeadingUploadSettings.AutoSize = true;
            this.lblHeadingUploadSettings.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingUploadSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.lblHeadingUploadSettings.Location = new System.Drawing.Point(17, 14);
            this.lblHeadingUploadSettings.Name = "lblHeadingUploadSettings";
            this.lblHeadingUploadSettings.Size = new System.Drawing.Size(336, 21);
            this.lblHeadingUploadSettings.TabIndex = 19;
            this.lblHeadingUploadSettings.Text = "Choose how your screenshots will be uploaded";
            // 
            // lblFileNameScheme
            // 
            this.lblFileNameScheme.AutoSize = true;
            this.lblFileNameScheme.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileNameScheme.Location = new System.Drawing.Point(69, 119);
            this.lblFileNameScheme.Name = "lblFileNameScheme";
            this.lblFileNameScheme.Size = new System.Drawing.Size(100, 13);
            this.lblFileNameScheme.TabIndex = 18;
            this.lblFileNameScheme.Text = "File name scheme:";
            // 
            // lblUploadSettingsFileNameFormat
            // 
            this.lblUploadSettingsFileNameFormat.AutoSize = true;
            this.lblUploadSettingsFileNameFormat.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUploadSettingsFileNameFormat.Location = new System.Drawing.Point(18, 96);
            this.lblUploadSettingsFileNameFormat.Name = "lblUploadSettingsFileNameFormat";
            this.lblUploadSettingsFileNameFormat.Size = new System.Drawing.Size(116, 13);
            this.lblUploadSettingsFileNameFormat.TabIndex = 16;
            this.lblUploadSettingsFileNameFormat.Text = "File name and format";
            // 
            // pbUploadSettingsLocMethod
            // 
            this.pbUploadSettingsLocMethod.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbUploadSettingsLocMethod.Location = new System.Drawing.Point(176, 235);
            this.pbUploadSettingsLocMethod.Name = "pbUploadSettingsLocMethod";
            this.pbUploadSettingsLocMethod.Size = new System.Drawing.Size(330, 1);
            this.pbUploadSettingsLocMethod.TabIndex = 26;
            this.pbUploadSettingsLocMethod.TabStop = false;
            // 
            // pbUploadSettingsFileNameFormat
            // 
            this.pbUploadSettingsFileNameFormat.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbUploadSettingsFileNameFormat.Location = new System.Drawing.Point(136, 103);
            this.pbUploadSettingsFileNameFormat.Name = "pbUploadSettingsFileNameFormat";
            this.pbUploadSettingsFileNameFormat.Size = new System.Drawing.Size(370, 1);
            this.pbUploadSettingsFileNameFormat.TabIndex = 17;
            this.pbUploadSettingsFileNameFormat.TabStop = false;
            // 
            // mpKeyboardShortcuts
            // 
            this.mpKeyboardShortcuts.BackColor = System.Drawing.Color.White;
            this.mpKeyboardShortcuts.Controls.Add(this.panelClipboardAddon);
            this.mpKeyboardShortcuts.Controls.Add(this.lblDescKeyboardShortcuts);
            this.mpKeyboardShortcuts.Controls.Add(this.lblSelectionShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.lblActiveWindowShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.lblWholeDesktopShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.tbSelectionShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.tbActiveWindowShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.tbWholeDesktopShortcutKey);
            this.mpKeyboardShortcuts.Controls.Add(this.lblSelection);
            this.mpKeyboardShortcuts.Controls.Add(this.lblActiveWindow);
            this.mpKeyboardShortcuts.Controls.Add(this.lblWholeDesktop);
            this.mpKeyboardShortcuts.Controls.Add(this.lblHeadingKeyboardShortcuts);
            this.mpKeyboardShortcuts.Controls.Add(this.pbExampleSelection);
            this.mpKeyboardShortcuts.Controls.Add(this.pbExampleActiveWindow);
            this.mpKeyboardShortcuts.Controls.Add(this.pbExampleWholeDesktop);
            this.mpKeyboardShortcuts.Location = new System.Drawing.Point(4, 22);
            this.mpKeyboardShortcuts.Name = "mpKeyboardShortcuts";
            this.mpKeyboardShortcuts.Size = new System.Drawing.Size(506, 414);
            this.mpKeyboardShortcuts.TabIndex = 0;
            this.mpKeyboardShortcuts.Text = "managedPanel1";
            // 
            // panelClipboardAddon
            // 
            this.panelClipboardAddon.Controls.Add(this.pbExampleClipboard);
            this.panelClipboardAddon.Controls.Add(this.lblClipboardShortcutKey);
            this.panelClipboardAddon.Controls.Add(this.lblClipboard);
            this.panelClipboardAddon.Controls.Add(this.tbClipboardShortcutKey);
            this.panelClipboardAddon.Location = new System.Drawing.Point(38, 343);
            this.panelClipboardAddon.Name = "panelClipboardAddon";
            this.panelClipboardAddon.Size = new System.Drawing.Size(442, 65);
            this.panelClipboardAddon.TabIndex = 38;
            this.panelClipboardAddon.Visible = false;
            // 
            // pbExampleClipboard
            // 
            this.pbExampleClipboard.Image = ((System.Drawing.Image)(resources.GetObject("pbExampleClipboard.Image")));
            this.pbExampleClipboard.Location = new System.Drawing.Point(0, 0);
            this.pbExampleClipboard.Name = "pbExampleClipboard";
            this.pbExampleClipboard.Size = new System.Drawing.Size(114, 64);
            this.pbExampleClipboard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbExampleClipboard.TabIndex = 34;
            this.pbExampleClipboard.TabStop = false;
            // 
            // lblClipboardShortcutKey
            // 
            this.lblClipboardShortcutKey.AutoSize = true;
            this.lblClipboardShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClipboardShortcutKey.Location = new System.Drawing.Point(120, 31);
            this.lblClipboardShortcutKey.Name = "lblClipboardShortcutKey";
            this.lblClipboardShortcutKey.Size = new System.Drawing.Size(74, 13);
            this.lblClipboardShortcutKey.TabIndex = 37;
            this.lblClipboardShortcutKey.Text = "Shortcut key:";
            // 
            // lblClipboard
            // 
            this.lblClipboard.AutoSize = true;
            this.lblClipboard.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClipboard.Location = new System.Drawing.Point(120, 5);
            this.lblClipboard.Name = "lblClipboard";
            this.lblClipboard.Size = new System.Drawing.Size(196, 13);
            this.lblClipboard.TabIndex = 35;
            this.lblClipboard.Text = "Upload from the Windows clipboard";
            // 
            // tbClipboardShortcutKey
            // 
            this.tbClipboardShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbClipboardShortcutKey.Location = new System.Drawing.Point(200, 28);
            this.tbClipboardShortcutKey.Name = "tbClipboardShortcutKey";
            this.tbClipboardShortcutKey.Size = new System.Drawing.Size(239, 22);
            this.tbClipboardShortcutKey.TabIndex = 36;
            this.tbClipboardShortcutKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HotkeyBox_KeyDown);
            this.tbClipboardShortcutKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HotkeyBox_KeyPress);
            // 
            // lblDescKeyboardShortcuts
            // 
            this.lblDescKeyboardShortcuts.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescKeyboardShortcuts.Location = new System.Drawing.Point(18, 41);
            this.lblDescKeyboardShortcuts.Name = "lblDescKeyboardShortcuts";
            this.lblDescKeyboardShortcuts.Size = new System.Drawing.Size(484, 52);
            this.lblDescKeyboardShortcuts.TabIndex = 32;
            this.lblDescKeyboardShortcuts.Text = resources.GetString("lblDescKeyboardShortcuts.Text");
            // 
            // lblSelectionShortcutKey
            // 
            this.lblSelectionShortcutKey.AutoSize = true;
            this.lblSelectionShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectionShortcutKey.Location = new System.Drawing.Point(158, 293);
            this.lblSelectionShortcutKey.Name = "lblSelectionShortcutKey";
            this.lblSelectionShortcutKey.Size = new System.Drawing.Size(74, 13);
            this.lblSelectionShortcutKey.TabIndex = 31;
            this.lblSelectionShortcutKey.Text = "Shortcut key:";
            // 
            // lblActiveWindowShortcutKey
            // 
            this.lblActiveWindowShortcutKey.AutoSize = true;
            this.lblActiveWindowShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActiveWindowShortcutKey.Location = new System.Drawing.Point(158, 213);
            this.lblActiveWindowShortcutKey.Name = "lblActiveWindowShortcutKey";
            this.lblActiveWindowShortcutKey.Size = new System.Drawing.Size(74, 13);
            this.lblActiveWindowShortcutKey.TabIndex = 30;
            this.lblActiveWindowShortcutKey.Text = "Shortcut key:";
            // 
            // lblWholeDesktopShortcutKey
            // 
            this.lblWholeDesktopShortcutKey.AutoSize = true;
            this.lblWholeDesktopShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWholeDesktopShortcutKey.Location = new System.Drawing.Point(158, 134);
            this.lblWholeDesktopShortcutKey.Name = "lblWholeDesktopShortcutKey";
            this.lblWholeDesktopShortcutKey.Size = new System.Drawing.Size(74, 13);
            this.lblWholeDesktopShortcutKey.TabIndex = 29;
            this.lblWholeDesktopShortcutKey.Text = "Shortcut key:";
            // 
            // tbSelectionShortcutKey
            // 
            this.tbSelectionShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSelectionShortcutKey.Location = new System.Drawing.Point(238, 290);
            this.tbSelectionShortcutKey.Name = "tbSelectionShortcutKey";
            this.tbSelectionShortcutKey.Size = new System.Drawing.Size(239, 22);
            this.tbSelectionShortcutKey.TabIndex = 28;
            this.tbSelectionShortcutKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HotkeyBox_KeyDown);
            this.tbSelectionShortcutKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HotkeyBox_KeyPress);
            // 
            // tbActiveWindowShortcutKey
            // 
            this.tbActiveWindowShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbActiveWindowShortcutKey.Location = new System.Drawing.Point(238, 210);
            this.tbActiveWindowShortcutKey.Name = "tbActiveWindowShortcutKey";
            this.tbActiveWindowShortcutKey.Size = new System.Drawing.Size(239, 22);
            this.tbActiveWindowShortcutKey.TabIndex = 27;
            this.tbActiveWindowShortcutKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HotkeyBox_KeyDown);
            this.tbActiveWindowShortcutKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HotkeyBox_KeyPress);
            // 
            // tbWholeDesktopShortcutKey
            // 
            this.tbWholeDesktopShortcutKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWholeDesktopShortcutKey.Location = new System.Drawing.Point(241, 131);
            this.tbWholeDesktopShortcutKey.Name = "tbWholeDesktopShortcutKey";
            this.tbWholeDesktopShortcutKey.Size = new System.Drawing.Size(239, 22);
            this.tbWholeDesktopShortcutKey.TabIndex = 26;
            this.tbWholeDesktopShortcutKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HotkeyBox_KeyDown);
            this.tbWholeDesktopShortcutKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HotkeyBox_KeyPress);
            // 
            // lblSelection
            // 
            this.lblSelection.AutoSize = true;
            this.lblSelection.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelection.Location = new System.Drawing.Point(158, 267);
            this.lblSelection.Name = "lblSelection";
            this.lblSelection.Size = new System.Drawing.Size(106, 13);
            this.lblSelection.TabIndex = 25;
            this.lblSelection.Text = "Capture a selection";
            // 
            // lblActiveWindow
            // 
            this.lblActiveWindow.AutoSize = true;
            this.lblActiveWindow.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActiveWindow.Location = new System.Drawing.Point(158, 187);
            this.lblActiveWindow.Name = "lblActiveWindow";
            this.lblActiveWindow.Size = new System.Drawing.Size(145, 13);
            this.lblActiveWindow.TabIndex = 24;
            this.lblActiveWindow.Text = "Capture the active window";
            // 
            // lblWholeDesktop
            // 
            this.lblWholeDesktop.AutoSize = true;
            this.lblWholeDesktop.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWholeDesktop.Location = new System.Drawing.Point(158, 107);
            this.lblWholeDesktop.Name = "lblWholeDesktop";
            this.lblWholeDesktop.Size = new System.Drawing.Size(148, 13);
            this.lblWholeDesktop.TabIndex = 23;
            this.lblWholeDesktop.Text = "Capture the whole desktop";
            // 
            // lblHeadingKeyboardShortcuts
            // 
            this.lblHeadingKeyboardShortcuts.AutoSize = true;
            this.lblHeadingKeyboardShortcuts.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingKeyboardShortcuts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.lblHeadingKeyboardShortcuts.Location = new System.Drawing.Point(17, 14);
            this.lblHeadingKeyboardShortcuts.Name = "lblHeadingKeyboardShortcuts";
            this.lblHeadingKeyboardShortcuts.Size = new System.Drawing.Size(291, 21);
            this.lblHeadingKeyboardShortcuts.TabIndex = 19;
            this.lblHeadingKeyboardShortcuts.Text = "Change keyboard shortcut combinations";
            // 
            // pbExampleSelection
            // 
            this.pbExampleSelection.Image = ((System.Drawing.Image)(resources.GetObject("pbExampleSelection.Image")));
            this.pbExampleSelection.Location = new System.Drawing.Point(38, 262);
            this.pbExampleSelection.Name = "pbExampleSelection";
            this.pbExampleSelection.Size = new System.Drawing.Size(114, 64);
            this.pbExampleSelection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbExampleSelection.TabIndex = 22;
            this.pbExampleSelection.TabStop = false;
            // 
            // pbExampleActiveWindow
            // 
            this.pbExampleActiveWindow.Image = ((System.Drawing.Image)(resources.GetObject("pbExampleActiveWindow.Image")));
            this.pbExampleActiveWindow.Location = new System.Drawing.Point(38, 182);
            this.pbExampleActiveWindow.Name = "pbExampleActiveWindow";
            this.pbExampleActiveWindow.Size = new System.Drawing.Size(114, 64);
            this.pbExampleActiveWindow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbExampleActiveWindow.TabIndex = 21;
            this.pbExampleActiveWindow.TabStop = false;
            // 
            // pbExampleWholeDesktop
            // 
            this.pbExampleWholeDesktop.Image = ((System.Drawing.Image)(resources.GetObject("pbExampleWholeDesktop.Image")));
            this.pbExampleWholeDesktop.Location = new System.Drawing.Point(38, 102);
            this.pbExampleWholeDesktop.Name = "pbExampleWholeDesktop";
            this.pbExampleWholeDesktop.Size = new System.Drawing.Size(114, 64);
            this.pbExampleWholeDesktop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbExampleWholeDesktop.TabIndex = 20;
            this.pbExampleWholeDesktop.TabStop = false;
            // 
            // mpAddons
            // 
            this.mpAddons.BackColor = System.Drawing.Color.White;
            this.mpAddons.Controls.Add(this.lvAddons);
            this.mpAddons.Controls.Add(this.lblDescAddons);
            this.mpAddons.Controls.Add(this.lblHeadingAddons);
            this.mpAddons.Controls.Add(this.tsAddonsCommandBar);
            this.mpAddons.Location = new System.Drawing.Point(4, 22);
            this.mpAddons.Name = "mpAddons";
            this.mpAddons.Size = new System.Drawing.Size(506, 414);
            this.mpAddons.TabIndex = 0;
            this.mpAddons.Text = "managedPanel1";
            // 
            // lvAddons
            // 
            this.lvAddons.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvAddons.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvAddons.FullRowSelect = true;
            this.lvAddons.Location = new System.Drawing.Point(0, 117);
            this.lvAddons.MultiSelect = false;
            this.lvAddons.Name = "lvAddons";
            this.lvAddons.Size = new System.Drawing.Size(514, 322);
            this.lvAddons.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvAddons.TabIndex = 25;
            this.lvAddons.UseCompatibleStateImageBehavior = false;
            this.lvAddons.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            this.columnHeader2.Width = 390;
            // 
            // lblDescAddons
            // 
            this.lblDescAddons.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescAddons.Location = new System.Drawing.Point(18, 42);
            this.lblDescAddons.Name = "lblDescAddons";
            this.lblDescAddons.Size = new System.Drawing.Size(484, 30);
            this.lblDescAddons.TabIndex = 24;
            this.lblDescAddons.Text = "An addon is a component that adds or changes features of a program, such as the a" +
    "blity to upload from the Windows clipboard. You can install and configure addons" +
    " below.";
            // 
            // lblHeadingAddons
            // 
            this.lblHeadingAddons.AutoSize = true;
            this.lblHeadingAddons.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingAddons.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.lblHeadingAddons.Location = new System.Drawing.Point(17, 14);
            this.lblHeadingAddons.Name = "lblHeadingAddons";
            this.lblHeadingAddons.Size = new System.Drawing.Size(248, 21);
            this.lblHeadingAddons.TabIndex = 23;
            this.lblHeadingAddons.Text = "Extend the program\'s functionality";
            // 
            // tsAddonsCommandBar
            // 
            this.tsAddonsCommandBar.AutoSize = false;
            this.tsAddonsCommandBar.BackColor = System.Drawing.Color.Azure;
            this.tsAddonsCommandBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tsAddonsCommandBar.BackgroundImage")));
            this.tsAddonsCommandBar.Dock = System.Windows.Forms.DockStyle.None;
            this.tsAddonsCommandBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsAddonsCommandBar.Location = new System.Drawing.Point(0, 85);
            this.tsAddonsCommandBar.Name = "tsAddonsCommandBar";
            this.tsAddonsCommandBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsAddonsCommandBar.Size = new System.Drawing.Size(514, 33);
            this.tsAddonsCommandBar.TabIndex = 26;
            this.tsAddonsCommandBar.Text = "toolStrip1";
            // 
            // mpAboutGrabbie
            // 
            this.mpAboutGrabbie.BackColor = System.Drawing.Color.White;
            this.mpAboutGrabbie.Controls.Add(this.lblHeadingAbout);
            this.mpAboutGrabbie.Controls.Add(this.lnkWebsite);
            this.mpAboutGrabbie.Controls.Add(this.mltbLicence);
            this.mpAboutGrabbie.Controls.Add(this.pbLicence);
            this.mpAboutGrabbie.Controls.Add(this.lblLicence);
            this.mpAboutGrabbie.Controls.Add(this.lblBuildTag);
            this.mpAboutGrabbie.Controls.Add(this.lblCopyright);
            this.mpAboutGrabbie.Controls.Add(this.lblProductName);
            this.mpAboutGrabbie.Controls.Add(this.pbVersion);
            this.mpAboutGrabbie.Controls.Add(this.lblVersion);
            this.mpAboutGrabbie.Controls.Add(this.pbProductLogo);
            this.mpAboutGrabbie.Location = new System.Drawing.Point(4, 22);
            this.mpAboutGrabbie.Name = "mpAboutGrabbie";
            this.mpAboutGrabbie.Size = new System.Drawing.Size(506, 414);
            this.mpAboutGrabbie.TabIndex = 0;
            this.mpAboutGrabbie.Text = "managedPanel1";
            // 
            // lblHeadingAbout
            // 
            this.lblHeadingAbout.AutoSize = true;
            this.lblHeadingAbout.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.lblHeadingAbout.Location = new System.Drawing.Point(17, 14);
            this.lblHeadingAbout.Name = "lblHeadingAbout";
            this.lblHeadingAbout.Size = new System.Drawing.Size(267, 21);
            this.lblHeadingAbout.TabIndex = 11;
            this.lblHeadingAbout.Text = "View information about this software";
            // 
            // lnkWebsite
            // 
            this.lnkWebsite.ActiveLinkColor = System.Drawing.Color.Blue;
            this.lnkWebsite.AutoSize = true;
            this.lnkWebsite.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkWebsite.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkWebsite.Location = new System.Drawing.Point(34, 135);
            this.lnkWebsite.Name = "lnkWebsite";
            this.lnkWebsite.Size = new System.Drawing.Size(211, 13);
            this.lnkWebsite.TabIndex = 10;
            this.lnkWebsite.TabStop = true;
            this.lnkWebsite.Text = "Get more NasuTek products on the web";
            this.lnkWebsite.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // mltbLicence
            // 
            this.mltbLicence.BackColor = System.Drawing.Color.White;
            this.mltbLicence.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mltbLicence.Location = new System.Drawing.Point(37, 225);
            this.mltbLicence.Multiline = true;
            this.mltbLicence.Name = "mltbLicence";
            this.mltbLicence.ReadOnly = true;
            this.mltbLicence.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mltbLicence.Size = new System.Drawing.Size(455, 197);
            this.mltbLicence.TabIndex = 9;
            this.mltbLicence.Text = resources.GetString("mltbLicence.Text");
            // 
            // pbLicence
            // 
            this.pbLicence.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbLicence.Location = new System.Drawing.Point(62, 206);
            this.pbLicence.Name = "pbLicence";
            this.pbLicence.Size = new System.Drawing.Size(444, 1);
            this.pbLicence.TabIndex = 8;
            this.pbLicence.TabStop = false;
            // 
            // lblLicence
            // 
            this.lblLicence.AutoSize = true;
            this.lblLicence.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicence.Location = new System.Drawing.Point(17, 199);
            this.lblLicence.Name = "lblLicence";
            this.lblLicence.Size = new System.Drawing.Size(44, 13);
            this.lblLicence.TabIndex = 7;
            this.lblLicence.Text = "Licence";
            // 
            // lblBuildTag
            // 
            this.lblBuildTag.AutoSize = true;
            this.lblBuildTag.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuildTag.Location = new System.Drawing.Point(34, 112);
            this.lblBuildTag.Name = "lblBuildTag";
            this.lblBuildTag.Size = new System.Drawing.Size(37, 13);
            this.lblBuildTag.TabIndex = 6;
            this.lblBuildTag.Text = "Build ";
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.Location = new System.Drawing.Point(34, 89);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(257, 13);
            this.lblCopyright.TabIndex = 5;
            this.lblCopyright.Text = "Copyright © 2012 NasuTek Software Foundation";
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.Location = new System.Drawing.Point(34, 66);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(97, 13);
            this.lblProductName.TabIndex = 4;
            this.lblProductName.Text = "NasuTek Grabbie ";
            // 
            // pbVersion
            // 
            this.pbVersion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pbVersion.Location = new System.Drawing.Point(65, 52);
            this.pbVersion.Name = "pbVersion";
            this.pbVersion.Size = new System.Drawing.Size(440, 1);
            this.pbVersion.TabIndex = 3;
            this.pbVersion.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(17, 45);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(46, 13);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "Version";
            // 
            // pbProductLogo
            // 
            this.pbProductLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbProductLogo.Image")));
            this.pbProductLogo.Location = new System.Drawing.Point(374, 66);
            this.pbProductLogo.Name = "pbProductLogo";
            this.pbProductLogo.Size = new System.Drawing.Size(128, 128);
            this.pbProductLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbProductLogo.TabIndex = 1;
            this.pbProductLogo.TabStop = false;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(714, 486);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelSideBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grabbie Settings";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbBottomPanelLine)).EndInit();
            this.panelSideBar.ResumeLayout(false);
            this.panelSideBar.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.mpBasicSettings.ResumeLayout(false);
            this.mpBasicSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSectionBasicSettingsProgressNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSectionBasicSettingsStartupPreview)).EndInit();
            this.mpUploadSettings.ResumeLayout(false);
            this.mpUploadSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUploadSettingsLocMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUploadSettingsFileNameFormat)).EndInit();
            this.mpKeyboardShortcuts.ResumeLayout(false);
            this.mpKeyboardShortcuts.PerformLayout();
            this.panelClipboardAddon.ResumeLayout(false);
            this.panelClipboardAddon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleClipboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleActiveWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbExampleWholeDesktop)).EndInit();
            this.mpAddons.ResumeLayout(false);
            this.mpAddons.PerformLayout();
            this.mpAboutGrabbie.ResumeLayout(false);
            this.mpAboutGrabbie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLicence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbProductLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideBar;
        private System.Windows.Forms.TabPage mpBasicSettings;
        private System.Windows.Forms.Label lblSeeAlso;
        private System.Windows.Forms.LinkLabel lnkAddons;
        private System.Windows.Forms.Label lblTasks;
        private System.Windows.Forms.LinkLabel lnkKeyboardSettings;
        private System.Windows.Forms.LinkLabel lnkUploadSettings;
        private System.Windows.Forms.LinkLabel lnkAboutGrabbie;
        private System.Windows.Forms.LinkLabel lnkBasicSettings;
        private System.Windows.Forms.TabPage mpUploadSettings;
        private System.Windows.Forms.TabPage mpKeyboardShortcuts;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox pbBottomPanelLine;
        private System.Windows.Forms.TabPage mpAboutGrabbie;
        private System.Windows.Forms.LinkLabel lnkWebsite;
        private System.Windows.Forms.TextBox mltbLicence;
        private System.Windows.Forms.PictureBox pbLicence;
        private System.Windows.Forms.Label lblLicence;
        private System.Windows.Forms.Label lblBuildTag;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.PictureBox pbVersion;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.PictureBox pbProductLogo;
        private System.Windows.Forms.Label lblHeadingAbout;
        private System.Windows.Forms.Label lblHeadingBasicSettings;
        private System.Windows.Forms.PictureBox pbSectionBasicSettingsStartupPreview;
        private System.Windows.Forms.Label lblSectionBasicSettingsStartupPreview;
        private System.Windows.Forms.Label lblHeadingUploadSettings;
        private System.Windows.Forms.Label lblFileNameScheme;
        private System.Windows.Forms.PictureBox pbUploadSettingsFileNameFormat;
        private System.Windows.Forms.Label lblUploadSettingsFileNameFormat;
        private System.Windows.Forms.Label lblDescUploadSettings;
        private System.Windows.Forms.Label lblImageFileFormat;
        private System.Windows.Forms.ComboBox cbFileNameScheme;
        private System.Windows.Forms.RadioButton rbJPG;
        private System.Windows.Forms.RadioButton rbPNG;
        private System.Windows.Forms.ComboBox cbUploadMethod;
        private System.Windows.Forms.Label lblUploadMethod;
        private System.Windows.Forms.PictureBox pbUploadSettingsLocMethod;
        private System.Windows.Forms.Label lblUploadSettingsLocMethod;
        private System.Windows.Forms.TabPage mpAddons;
        private System.Windows.Forms.ListView lvAddons;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label lblDescAddons;
        private System.Windows.Forms.Label lblHeadingAddons;
        private System.Windows.Forms.ToolStrip tsAddonsCommandBar;
        private System.Windows.Forms.ToolStripButton cmdInstallAddon;
        private System.Windows.Forms.ToolStripButton cmdUninstallAddon;
        private System.Windows.Forms.ToolStripButton cmdEnableDisableAddon;
        private System.Windows.Forms.ToolStripButton cmdChangeAddonSettings;
        private System.Windows.Forms.Label lblDescKeyboardShortcuts;
        private System.Windows.Forms.Label lblSelectionShortcutKey;
        private System.Windows.Forms.Label lblActiveWindowShortcutKey;
        private System.Windows.Forms.Label lblWholeDesktopShortcutKey;
        private System.Windows.Forms.TextBox tbSelectionShortcutKey;
        private System.Windows.Forms.TextBox tbActiveWindowShortcutKey;
        private System.Windows.Forms.TextBox tbWholeDesktopShortcutKey;
        private System.Windows.Forms.Label lblSelection;
        private System.Windows.Forms.Label lblActiveWindow;
        private System.Windows.Forms.Label lblWholeDesktop;
        private System.Windows.Forms.PictureBox pbExampleSelection;
        private System.Windows.Forms.PictureBox pbExampleActiveWindow;
        private System.Windows.Forms.PictureBox pbExampleWholeDesktop;
        private System.Windows.Forms.Label lblHeadingKeyboardShortcuts;
        private System.Windows.Forms.Label lblClipboardShortcutKey;
        private System.Windows.Forms.TextBox tbClipboardShortcutKey;
        private System.Windows.Forms.Label lblClipboard;
        private System.Windows.Forms.PictureBox pbExampleClipboard;
        private System.Windows.Forms.Label lblProgressStyle;
        private System.Windows.Forms.ComboBox cbStatusType;
        private System.Windows.Forms.CheckBox ckbxCloseProgress;
        private System.Windows.Forms.CheckBox ckbxShowPreview;
        private System.Windows.Forms.CheckBox ckbxStartOnLogon;
        private System.Windows.Forms.PictureBox pbSectionBasicSettingsProgressNotifications;
        private System.Windows.Forms.Label lblSectionBasicSettingsProgressNotifications;
        private System.Windows.Forms.Label lblDescBasicSettings;
        private NasuTek.Grabbie.CustomTabControl tabControl1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ComboBox cb;
        private System.Windows.Forms.Label lblUrlShortner;
        private System.Windows.Forms.Label lblUploadSettingsUrlShortner;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelClipboardAddon;
    }
}

