﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie {
    public partial class StatusForm : Form, IUploadNotificationSystem {
        public StatusForm() {
            InitializeComponent();
            HideShowDetails();
        }

        public void SetUploadStatus(UploadStatus status) {
            switch (status) {
                case UploadStatus.Uploading:
                    Show();
                    AddToUploadLog("---Beginning of Upload " + DateTime.Now + " ---");
                    Program.Core.RollTheWheel();
                    break;
                case UploadStatus.Success:
                    Invoke(new Action(HideForm));
                    Program.Core.DealDeck(status);
                    break;
                case UploadStatus.Error:
                    Program.Core.DealDeck(status);
                    break;
            }
        }

        public void UpdateStatus(string statusMessage, int process) {
            Invoke(new Action(() => {
                label1.Text = statusMessage;
                progressBar1.Value = process;
            }));
        }

        public void AddToUploadLog(string text) {
            Invoke(new Action(() => textBox1.AppendText(text + "\n")));
        }

        private void button1_Click(object sender, EventArgs e) {
            Hide();
        }

        private void HideForm() {
            if (PropertyService.Get("Grabbie.BasicSettings.HideUploadWindow", true)) Hide();
        }

        private void StatusForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            HideShowDetails();
        }

        private void HideShowDetails() {
            if (button2.Text == "Details >>") {
                button2.Text = "<< Details";
                Height = 445;
                CenterToScreen();
                panel1.Visible = true;
            } else {
                button2.Text = "Details >>";
                Height = 175;
                CenterToScreen();
                panel1.Visible = false;
            }
        }
    }
}