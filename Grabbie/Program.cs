﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces;
using NasuTek.Grabbie.InternalNotificationEngines;

#endregion

namespace NasuTek.Grabbie {
    internal static class Program {
        internal static GrabbieEngine Engine { get; private set; }
        internal static Grabbie Core { get; private set; }

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread] private static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoggingService.Info("Application start");
            var exe = typeof (Program).Assembly;
            FileUtility.ApplicationRootPath = Path.GetDirectoryName(exe.Location);

            LoggingService.Info("Starting core services...");

            var coreStartup = new CoreStartup("NasuTek Grabbie") {
                PropertiesName = "AppProperties"
            };

            if (Directory.Exists(Path.Combine(FileUtility.ApplicationRootPath, "Config")))
                coreStartup.ConfigDirectory = Path.Combine(FileUtility.ApplicationRootPath, "Config");

            coreStartup.StartCoreServices();

            LoggingService.Info("Looking for AddIns...");

            coreStartup.AddAddInsFromDirectory(Path.Combine(FileUtility.ApplicationRootPath, "Plugins"));
            coreStartup.ConfigureExternalAddIns(Path.Combine(PropertyService.ConfigDirectory, "AddIns.xml"));
            coreStartup.ConfigureUserAddIns(
                Path.Combine(PropertyService.ConfigDirectory, "AddInInstallTemp"),
                Path.Combine(PropertyService.ConfigDirectory, "AddIns"));

            LoggingService.Info("Loading AddInTree...");
            coreStartup.RunInitialization();

            LoggingService.Info("Initializing Grabbie Engine...");
            Engine = new GrabbieEngine {
                UploadNotificationSystem = new StatusForm()
            };

            Engine.AlertTypes.Add("None", new NeNone());
            Engine.AlertTypes.Add("System Default", new NeBalloon());

            if (AddInTree.ExistsTreeNode("/Grabbie/CaptureServices")) {
                foreach (
                    var obj in
                        AddInTree.GetTreeNode("/Grabbie/CaptureServices").Codons.Select(
                            codon => codon.AddIn.CreateObject(codon.Properties["class"])).OfType<ICaptureType>()) {
                    obj.Hotkey.Shortcut =
                        PropertyService.Get("Grabbie.Hotkey." + obj.Hotkey.ShortcutName.Replace(" ", ""),
                            obj.Hotkey.Shortcut);
                    Engine.CaptureTypes.Add(obj);
                }
            }

            if (AddInTree.ExistsTreeNode("/Grabbie/FileNameServices")) {
                foreach (var codon in AddInTree.GetTreeNode("/Grabbie/FileNameServices").Codons) {
                    var obj = codon.AddIn.CreateObject(codon.Properties["class"]) as IFileNameType;
                    if (obj != null)
                        Engine.FileNameTypes.Add(codon.Properties["name"], obj);
                }
            }

            if (AddInTree.ExistsTreeNode("/Grabbie/UrlShornerServices")) {
                foreach (var codon in AddInTree.GetTreeNode("/Grabbie/UrlShornerServices").Codons) {
                    var obj = codon.AddIn.CreateObject(codon.Properties["class"]) as IUrlShortnerType;
                    if (obj != null)
                        Engine.UrlShortnerTypes.Add(codon.Properties["name"], obj);
                }
            }

            if (AddInTree.ExistsTreeNode("/Grabbie/Uploaders")) {
                foreach (var codon in AddInTree.GetTreeNode("/Grabbie/Uploaders").Codons) {
                    var obj = codon.AddIn.CreateObject(codon.Properties["class"]) as IUploadType;
                    if (obj != null)
                        Engine.UploadTypes.Add(codon.Properties["name"], obj);
                }
            }

            if (AddInTree.ExistsTreeNode("/Grabbie/Alerters")) {
                foreach (var codon in AddInTree.GetTreeNode("/Grabbie/Alerters").Codons) {
                    var obj = codon.AddIn.CreateObject(codon.Properties["class"]) as IAlertType;
                    if (obj != null)
                        Engine.AlertTypes.Add(codon.Properties["name"], obj);
                }
            }

            if (AddInTree.ExistsTreeNode("/Grabbie/SettingsPages")) {
                foreach (
                    var obj in
                        AddInTree.GetTreeNode("/Grabbie/SettingsPages").Codons.Select(
                            codon => codon.AddIn.CreateObject(codon.Properties["class"])).OfType<SettingsPage>())
                    Engine.SettingsPages.Add(obj);
            }

            LoggingService.Info("Initializing Grabbie Core...");
            Core = new Grabbie();

            LoggingService.Info("Running Grabbie...");

            try {
                Application.Run();
            } finally {
                try {
                    PropertyService.Save();
                } catch (Exception ex) {
                    MessageService.ShowException(ex, "Error storing properties");
                }
            }

            LoggingService.Info("Application shutdown");
        }
    }
}