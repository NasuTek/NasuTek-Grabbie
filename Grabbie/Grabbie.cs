﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using NasuTek.Grabbie.Interfaces;
using NasuTek.Grabbie.Properties;

#endregion

namespace NasuTek.Grabbie {
    public partial class Grabbie : Component {
        private bool m_Rolling;
        private Thread m_Wheelthread;

        public Grabbie() {
            InitializeComponent();
            Constuct();
        }

        public Grabbie(IContainer container) {
            container.Add(this);

            InitializeComponent();
            Constuct();
        }

        private void Constuct() {
            toolStripMenuItem1.Click += toolStripMenuItem1_Click;
            toolStripMenuItem2.Click += toolStripMenuItem2_Click;
            toolStripMenuItem3.Click += toolStripMenuItem3_Click;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e) {
            ((StatusForm) GrabbieEngine.Instance.UploadNotificationSystem).Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e) {
            new Settings().Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e) {
            notifyIcon1.Visible = false;
            Application.Exit();
        }

        public void RollTheWheel() {
            m_Wheelthread = new Thread(() => {
                try {
                    m_Rolling = true;
                    const int waitingtime = 300;
                    while (m_Rolling) {
                        ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(
                            new Action(() => notifyIcon1.Icon = Resources.tray_progress_1));
                        Thread.Sleep(waitingtime);
                        ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(
                            new Action(() => notifyIcon1.Icon = Resources.tray_progress_2));
                        Thread.Sleep(waitingtime);
                        ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(
                            new Action(() => notifyIcon1.Icon = Resources.tray_progress_3));
                        Thread.Sleep(waitingtime);
                    }
                } catch {}
            });
            m_Wheelthread.SetApartmentState(ApartmentState.STA);
            m_Wheelthread.Start();
        }

        public void DealDeck(UploadStatus status) {
            m_Rolling = false;
            m_Wheelthread.Abort();
            Thread.Sleep(300);
            ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(status == UploadStatus.Error
                ? (() => notifyIcon1.Icon = Resources.error) : new Action(() => notifyIcon1.Icon = Resources.success));
            Thread.Sleep(1000);
            ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(
                new Action(() => notifyIcon1.Icon = Resources.grabbie));
        }

        public void ShowBalloon(BalloonType baloonType, string title, string message, int timeout) {
            switch (baloonType) {
                case BalloonType.Error:
                    notifyIcon1.ShowBalloonTip(timeout, title, message, ToolTipIcon.Error);
                    break;
                case BalloonType.Exclamation:
                    notifyIcon1.ShowBalloonTip(timeout, title, message, ToolTipIcon.Warning);
                    break;
                case BalloonType.Information:
                    notifyIcon1.ShowBalloonTip(timeout, title, message, ToolTipIcon.Info);
                    break;
                default:
                    notifyIcon1.ShowBalloonTip(timeout, title, message, ToolTipIcon.None);
                    break;
            }
        }
    }
}