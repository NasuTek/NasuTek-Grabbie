﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ICSharpCode.Core;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie {
    public partial class Settings : Form {
        public Settings() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            lblBuildTag.Text += GlobalAssemblyInfo.FullVersion;
            lblProductName.Text += GlobalAssemblyInfo.CleanVersion;

            foreach (var item in AddInTree.AddIns.Select(addIn => new ListViewItem(new[] {
                addIn.Name, addIn.Properties["description"]
            }) {
                Tag = addIn
            }))
                lvAddons.Items.Add(item);

            if (cbStatusType.SelectedIndex == 2) {
                ckbxCloseProgress.Checked = false;
                ckbxCloseProgress.Enabled = false;
            } else
                ckbxCloseProgress.Enabled = true;

            foreach (var urlShortnerType in GrabbieEngine.Instance.UrlShortnerTypes)
                cb.Items.Add(urlShortnerType.Key);

            foreach (var fileNameType in GrabbieEngine.Instance.FileNameTypes)
                cbFileNameScheme.Items.Add(fileNameType.Key);

            foreach (var uploadType in GrabbieEngine.Instance.UploadTypes)
                cbUploadMethod.Items.Add(uploadType.Key);

            foreach (var alertType in GrabbieEngine.Instance.AlertTypes)
                cbStatusType.Items.Add(alertType.Key);

            cb.Text = PropertyService.Get("Grabbie.BasicSettings.URLShortner", "None");
            cbFileNameScheme.Text = PropertyService.Get("Grabbie.BasicSettings.FileNameType", "GUID File Name");
            cbUploadMethod.Text = PropertyService.Get("Grabbie.BasicSettings.UploadService");
            cbStatusType.Text = PropertyService.Get("Grabbie.BasicSettings.AlertType", "System Default");
            ckbxShowPreview.Checked = PropertyService.Get("Grabbie.BasicSettings.PreviewBeforeUpload", false);
            ckbxCloseProgress.Checked = PropertyService.Get("Grabbie.BasicSettings.HideUploadWindow", true);

            var imgFormat = PropertyService.Get("Grabbie.BasicSettings.ImageFormat", "JPG");
            if (imgFormat == "JPG")
                rbJPG.Checked = true;
            else
                rbPNG.Checked = true;

            var reg = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
            ckbxStartOnLogon.Checked = reg.GetValue("Grabbie") != null;

            tbSelectionShortcutKey.Text = GetHotkey("Selection Grab");
            tbActiveWindowShortcutKey.Text = GetHotkey("Active Window");
            tbWholeDesktopShortcutKey.Text = GetHotkey("Full Desktop");
        }

        private void lnkBasicSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            tabControl1.SelectedIndex = 0;
        }

        private void lnkUploadSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            tabControl1.SelectedIndex = 1;
        }

        private void lnkKeyboardSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            tabControl1.SelectedIndex = 2;
        }

        private void lnkAddons_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            tabControl1.SelectedIndex = 3;
        }

        private void lnkAboutGrabbie_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            tabControl1.SelectedIndex = 4;
        }

        private void cbStatusType_SelectedIndexChanged(object sender, EventArgs e) {
            if (cbStatusType.SelectedIndex == 2) {
                ckbxCloseProgress.Checked = false;
                ckbxCloseProgress.Enabled = false;
            } else ckbxCloseProgress.Enabled = true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            new AdvancedSettings().ShowDialog();
        }

        private void btnSave_Click(object sender, EventArgs e) {
            PropertyService.Set("Grabbie.BasicSettings.URLShortner", cb.Text);
            PropertyService.Set("Grabbie.BasicSettings.FileNameType", cbFileNameScheme.Text);
            PropertyService.Set("Grabbie.BasicSettings.UploadService", cbUploadMethod.Text);
            PropertyService.Set("Grabbie.BasicSettings.AlertType", cbStatusType.Text);
            PropertyService.Set("Grabbie.BasicSettings.PreviewBeforeUpload", ckbxShowPreview.Checked);
            PropertyService.Set("Grabbie.BasicSettings.HideUploadWindow", ckbxCloseProgress.Checked);
            PropertyService.Set("Grabbie.BasicSettings.ImageFormat", rbPNG.Checked ? "PNG" : "JPG");

            if (ckbxStartOnLogon.Checked) {
                var reg = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run",
                    true);

                if (reg.GetValue("Grabbie") == null)
                    reg.SetValue("Grabbie", Application.ExecutablePath);
            } else {
                var reg = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run",
                    true);
                if (reg.GetValue("Grabbie") != null)
                    reg.DeleteValue("Grabbie");
            }

            SetHotkey("Selection Grab", tbSelectionShortcutKey.Text);
            SetHotkey("Active Window", tbActiveWindowShortcutKey.Text);
            SetHotkey("Full Desktop", tbWholeDesktopShortcutKey.Text);

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }

        private void mpUploadSettings_Click(object sender, EventArgs e) {}

        private void cmdInstallAddon_Click(object sender, EventArgs e) {
            var ofd = new OpenFileDialog {
                Filter = "NasuTek Grabbie Plug-In Package (*.apui)|*.apui"
            };

            if (ofd.ShowDialog() == DialogResult.OK) {
                var fz = new FastZip();
                fz.ExtractZip(ofd.FileName, AddInManager.UserAddInPath, "");
                MessageBox.Show("Plug-in Installed, Please restart Grabbie to complete Installation.", "Grabbie",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #region Hotkey Setter Functions
        public void SetHotkey(string hotkeyName, string hotkeyText) {
            var hotkey = GrabbieEngine.Instance.GetCaptureTypeByHotkeyName(hotkeyName).Hotkey;
            hotkey.Shortcut = (Shortcut) Enum.Parse(typeof (Shortcut), ConverttoHotkey(hotkeyText));
            PropertyService.Set("Grabbie.Hotkey." + hotkeyName.Replace(" ", ""), hotkey.Shortcut);
        }

        public string GetHotkey(string hotkeyName) {
            var hotkey = GrabbieEngine.Instance.GetCaptureTypeByHotkeyName(hotkeyName).Hotkey;
            return ConvertfromHotkey(hotkey.Shortcut.ToString());
        }

        private void HotkeyBox_KeyDown(object sender, KeyEventArgs e) {
            e.Handled = true;
            string keyhook;
            if (e.KeyCode == Keys.Back)
                ((TextBox) sender).Text = "Alt + Back";
            if (e.KeyCode.ToString() == "Insert")
                ((TextBox) sender).Text = "Insert";
            if (e.KeyCode.ToString() == "Delete")
                ((TextBox) sender).Text = "Delete";
            /*if (e.KeyCode.ToString().Substring(0, 1) == "F" && e.KeyCode.ToString().Length > 1 && !e.Shift && !e.Control && !e.Alt) {
                keyhook = e.KeyCode.ToString ();
                if (Validatehotkey (keyhook) == false) {
                    keyhook = "None";
                }
                textBox7.Text = keyhook;
            }*/
            if (e.Control) {
                keyhook = "Ctrl";
                if (e.Shift) {
                    keyhook = "Ctrl + Shift";
                    if (e.KeyCode.ToString() != "ShiftKey") {
                        var ichar = e.KeyCode.ToString();

                        if (ichar.Contains("D") && ichar.Length == 2)
                            ichar = Regex.Replace(ichar, "D", String.Empty);
                        keyhook = "Ctrl + Shift + " + ichar;
                        if (Validatehotkey(keyhook) == false)
                            keyhook = "Ctrl + Shift";
                    }
                }
                if (e.KeyCode.ToString() != "ControlKey" && !e.Shift) {
                    var ichar = e.KeyCode.ToString();

                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Ctrl + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Ctrl";
                }
                ((TextBox) sender).Text = keyhook;
            }

            if (e.Alt) {
                keyhook = "Alt";
                if (e.KeyCode.ToString() != "Menu") {
                    var ichar = e.KeyCode.ToString();
                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Alt + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Alt";
                }
                ((TextBox) sender).Text = keyhook;
            }
            if (e.Shift && !e.Control) {
                keyhook = "Shift";
                if (e.KeyCode.ToString() != "ShiftKey" && !e.Control) {
                    var ichar = e.KeyCode.ToString();

                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Shift + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Shift";
                }
                ((TextBox) sender).Text = keyhook;
            }
        }

        private void HotkeyBox_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = true;
        }

        private bool IsNumeric(string numericstring) {
            return Regex.IsMatch(numericstring, "\\d+");
        }

        private bool Validatehotkey(string hotkey) {
            var isvalid = false;
            try {
                hotkey = Regex.Replace(hotkey, " ", String.Empty);
                var parts = hotkey.Split('+');
                var part1 = parts[0];
                var part2 = parts[1];
                var part3 = parts[parts.Length - 1];
                if (part1.Substring(0, 1) == "F" && part1.Length > 1)
                    isvalid = true;
                if (part1 == "Insert")
                    isvalid = true;
                if (part1 == "Delete")
                    isvalid = true;
                if (part1 == "Ctrl") {
                    if (part2 == "Shift") {
                        if (IsNumeric(part3))
                            isvalid = true;
                        if (Regex.IsMatch(part3, "^[A-Za-z]$"))
                            isvalid = true;
                        if (part3.Substring(0, 1) == "F" && part3.Length > 1)
                            isvalid = true;
                    } else {
                        if (IsNumeric(part2))
                            isvalid = true;
                        if (part2 == "Delete")
                            isvalid = true;
                        if (part2 == "Insert")
                            isvalid = true;
                        if (Regex.IsMatch(part2, "^[A-Za-z]$"))
                            isvalid = true;
                        if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                            isvalid = true;
                    }
                }
                if (part1 == "Alt") {
                    if (IsNumeric(part2))
                        isvalid = true;
                    if (part2 == "Down")
                        isvalid = true;
                    if (part2 == "Up")
                        isvalid = true;
                    if (part2 == "Left")
                        isvalid = true;
                    if (part2 == "Right")
                        isvalid = true;
                    if (part2 == "Back")
                        isvalid = true;
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        isvalid = true;
                }
                if (part1 == "Shift") {
                    if (part2 == "Insert")
                        isvalid = true;
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        isvalid = true;
                }
            } catch {}
            return isvalid;
        }

        private string ConverttoHotkey(string hotkey) {
            var realhotkey = "None";
            try {
                hotkey = Regex.Replace(hotkey, " ", String.Empty);
                var parts = hotkey.Split('+');
                var part1 = parts[0];
                var part2 = parts[1];
                var part3 = parts[parts.Length - 1];
                if (part1.Substring(0, 1) == "F" && part1.Length > 1)
                    realhotkey = part1;
                if (part1 == "Insert")
                    realhotkey = part1;
                if (part1 == "Delete")
                    realhotkey = part1;
                if (part1 == "Ctrl") {
                    if (part2 == "Shift") {
                        if (IsNumeric(part3))
                            realhotkey = part1 + part2 + part3;
                        if (Regex.IsMatch(part3, "^[A-Za-z]$"))
                            realhotkey = part1 + part2 + part3;
                        if (part3.Substring(0, 1) == "F" && part3.Length > 1)
                            realhotkey = part1 + part2 + part3;
                    } else {
                        if (IsNumeric(part2))
                            realhotkey = part1 + part2;
                        if (part2 == "Delete")
                            realhotkey = part1 + "Del";
                        if (part2 == "Insert")
                            realhotkey = part1 + "Ins";
                        if (Regex.IsMatch(part2, "^[A-Za-z]$"))
                            realhotkey = part1 + part2;
                        if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                            realhotkey = part1 + part2;
                    }
                }
                if (part1 == "Alt") {
                    if (IsNumeric(part2))
                        realhotkey = part1 + part2;
                    if (part2 == "Down")
                        realhotkey = part1 + "DownArrow";
                    if (part2 == "Up")
                        realhotkey = part1 + "UpArrow";
                    if (part2 == "Left")
                        realhotkey = part1 + "LeftArrow";
                    if (part2 == "Right")
                        realhotkey = part1 + "RightArrow";
                    if (part2 == "Back")
                        realhotkey = part1 + "Bksp";
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        realhotkey = part1 + part2;
                }
                if (part1 == "Shift") {
                    if (part2 == "Insert")
                        realhotkey = part1 + "Ins";
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        realhotkey = part1 + part2;
                }
            } catch {}
            return realhotkey;
        }

        internal string ConvertfromHotkey(string hotkey) {
            var realhotkey = "None";

            if (hotkey.Substring(0, 4) == "Ctrl") {
                if (hotkey.Length > 9 && hotkey.Substring(0, 9) == "CtrlShift") {
                    if (IsNumeric(hotkey.Substring(9, 1)))
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, 1);
                    if (Regex.IsMatch(hotkey.Substring(9, 1), "^[A-Za-z]$"))
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, 1);
                    if (hotkey.Substring(9, 1) == "F")
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, hotkey.Length - 9);
                } else {
                    if (IsNumeric(hotkey.Substring(4, 1)))
                        realhotkey = "Ctrl + " + hotkey.Substring(4, 1);
                    if (Regex.IsMatch(hotkey.Substring(4, 1), "^[A-Za-z]$"))
                        realhotkey = "Ctrl + " + hotkey.Substring(4, 1);
                    if (hotkey.Substring(0, 4)[1].ToString(CultureInfo.InvariantCulture) == "Del")
                        realhotkey = "Ctrl + Delete";
                    if (hotkey.Substring(0, 4)[1].ToString(CultureInfo.InvariantCulture) == "Ins")
                        realhotkey = "Ctrl + Insert";
                    if (hotkey.Substring(4, 1) == "F")
                        realhotkey = "Ctrl + " + hotkey.Substring(4, hotkey.Length - 4);
                }
            }
            if (hotkey.Substring(0, 3) == "Alt") {
                if (IsNumeric(hotkey.Substring(3, 1)))
                    realhotkey = "Alt + " + hotkey.Substring(3, 1);
                if (hotkey.Substring(3, hotkey.Length - 3) == "DownArrow")
                    realhotkey = "Alt + Down";
                if (hotkey.Substring(3, hotkey.Length - 3) == "UpArrow")
                    realhotkey = "Alt + Up";
                if (hotkey.Substring(3, hotkey.Length - 3) == "LeftArrow")
                    realhotkey = "Alt + Left";
                if (hotkey.Substring(3, hotkey.Length - 3) == "RightArrow")
                    realhotkey = "Alt + Right";
                if (hotkey.Substring(3, hotkey.Length - 3) == "Bksp")
                    realhotkey = "Alt + Back";
                if (hotkey.Substring(3, 1) == "F")
                    realhotkey = "Alt + " + hotkey.Substring(3, hotkey.Length - 3);
            }
            if (hotkey.Length > 4 && hotkey.Substring(0, 5) == "Shift") {
                if (hotkey.Substring(5, hotkey.Length - 5) == "Ins")
                    realhotkey = "Shift + Insert";
                if (hotkey.Substring(5, 1) == "F")
                    realhotkey = "Shift + " + hotkey.Substring(5, hotkey.Length - 5);
            }
            if (hotkey.Substring(0, 1) == "F" && hotkey.Length > 1)
                realhotkey = hotkey;

            return realhotkey;
        }
        #endregion
    }
}