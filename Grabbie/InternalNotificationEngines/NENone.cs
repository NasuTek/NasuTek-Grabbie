﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.InternalNotificationEngines {
    public class NeNone : IAlertType {
        public void ShowBalloon(BalloonType baloonType, string title, string message, int timeout) {}
    }

    public class NeBalloon : IAlertType {
        public void ShowBalloon(BalloonType baloonType, string title, string message, int timeout) {
            ((StatusForm) Program.Engine.UploadNotificationSystem).Invoke(
                new Action(() => Program.Core.ShowBalloon(baloonType, title, message, timeout)));
        }
    }
}