﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSharpCode.Core;

#endregion

namespace NasuTek.Grabbie.Globals {
    public partial class PiGeneral : UserControl {
        public PiGeneral(AddIn addIn) {
            InitializeComponent();
            textBox1.Text = addIn.Name;
            textBox2.Text = addIn.Properties["author"];
            textBox3.Text = addIn.Properties["url"];
            textBox4.Text = addIn.Properties["description"];
            textBox5.Text = addIn.Version.ToString();

            if (addIn.Properties["required"] == "true")
                comboBox1.Enabled = false;

            switch (addIn.Action) {
                case AddInAction.Enable:
                    comboBox1.Text = "Enabled";
                    break;
                case AddInAction.Disable:
                    comboBox1.Text = "Disabled";
                    break;
            }
        }

        public bool IsEnabled {
            get {
                switch (comboBox1.Text) {
                    case "Enabled":
                        return true;
                    case "Disabled":
                        return false;
                    default:
                        return false;
                }
            }
        }
    }
}