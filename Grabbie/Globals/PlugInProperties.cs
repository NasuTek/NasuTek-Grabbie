﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSharpCode.Core;

#endregion

namespace NasuTek.Grabbie.Globals {
    public partial class PlugInProperties : Form {
        private readonly AddIn m_AddIn;

        public PlugInProperties(AddIn addIn) {
            m_AddIn = addIn;

            InitializeComponent();
            treeView1.Nodes[0].Tag = new PiGeneral(addIn);

            Text = addIn.Name + " Properties";

            if (addIn.Properties["required"] == "true")
                button3.Enabled = false;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
            if (e.Node.Tag != null) {
                panel1.Controls.Clear();
                panel1.Controls.Add((Control) e.Node.Tag);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            if (m_AddIn.Enabled != ((PiGeneral) treeView1.Nodes[0].Tag).IsEnabled) {
                if (!((PiGeneral) treeView1.Nodes[0].Tag).IsEnabled) {
                    AddInManager.Disable(new[] {
                        m_AddIn
                    });
                } else {
                    AddInManager.Enable(new[] {
                        m_AddIn
                    });
                }
            }
            Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            Close();
        }

        private void button3_Click(object sender, EventArgs e) {
            if (
                MessageBox.Show("Are you sure you want to remove the Plug-in \"" + m_AddIn.Name + "\"?",
                    "NasuTek Grabbie", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) == DialogResult.Yes) {}
        }
    }
}