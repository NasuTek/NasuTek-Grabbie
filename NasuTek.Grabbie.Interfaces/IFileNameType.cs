﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     Used to generate a file name for uploading to the server. IFileNameType go into
    ///     /Grabbie/FileNameServices in the Addin Description File.
    /// </summary>
    public interface IFileNameType {
        /// <summary>
        ///     Called by Grabbie when ready for a filename.
        /// </summary>
        /// <returns>The randomally generated filename by this IFileNameType</returns>
        string GenerateFileName();
    }
}