﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces.ImagingServices {
    /// <summary>
    ///     Grabbing Services
    /// </summary>
    public class GrabbieImagingServices {
        /// <summary>
        ///     Grabs the image of the screen from this rectangle object
        /// </summary>
        /// <param name="rect">Screen Bounds to Grab</param>
        /// <returns>Grabbed Image</returns>
        public static Image Grab(Rectangle rect) {
            var sBit = new Bitmap(rect.Width, rect.Height);
            var sGraph = Graphics.FromImage(sBit);
            sGraph.CopyFromScreen(rect.Left, rect.Top, 0, 0, rect.Size);
            return sBit;
        }
    }
}