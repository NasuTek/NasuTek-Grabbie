﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     Shortens the URL for easy transfer. IUrlShortnerType go into /Grabbie/UrlShornerServices
    ///     in the Addin Description File.
    /// </summary>
    public interface IUrlShortnerType {
        /// <summary>
        ///     Shortens the URL.
        /// </summary>
        /// <param name="fullUrl">The URL to shorten.</param>
        /// <returns>Shortened URL.</returns>
        string ShortenUrl(string fullUrl);
    }
}