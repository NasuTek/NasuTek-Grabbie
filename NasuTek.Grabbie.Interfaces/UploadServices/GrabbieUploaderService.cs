﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ICSharpCode.Core;

#endregion

namespace NasuTek.Grabbie.Interfaces.UploadServices {
    /// <summary>
    ///     Uploads Images to the active Grab Uploading Engine
    /// </summary>
    public class GrabbieUploaderService {
        private static bool s_InUse;

        private static ImageCodecInfo GetEncoderInfo(string mimeType) {
            // Get image codecs for all image formats
            var codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            return codecs.FirstOrDefault(t => t.MimeType == mimeType);
        }

        /// <summary>
        ///     Uploads the image to the active Grab Uploading Engine
        /// </summary>
        /// <param name="imageToUpload">Grab Image</param>
        public static void UploadCapture(Image imageToUpload) {
            if (!s_InUse) {
                s_InUse = true;
                GrabbieEngine.Instance.UploadNotificationSystem.SetUploadStatus(UploadStatus.Uploading);

                var thred = new Thread(() => {
                    try {
                        var fileNamer =
                            GrabbieEngine.Instance.FileNameTypes[
                                PropertyService.Get("Grabbie.BasicSettings.FileNameType")];
                        var urlShortner =
                            GrabbieEngine.Instance.UrlShortnerTypes[
                                PropertyService.Get("Grabbie.BasicSettings.URLShortner")];
                        var uploadEngine =
                            GrabbieEngine.Instance.UploadTypes[
                                PropertyService.Get("Grabbie.BasicSettings.UploadService")];

                        string tempPath;

                        if (PropertyService.Get("Grabbie.BasicSettings.ImageFormat") == "JPG") {
                            tempPath = Path.Combine(Path.GetTempPath(), fileNamer.GenerateFileName() + ".jpg");

                            var qualityParam = new EncoderParameter(Encoder.Quality, 100L);
                            var jpegCodec = GetEncoderInfo("image/jpeg");

                            var encoderParams = new EncoderParameters(1);
                            encoderParams.Param[0] = qualityParam;

                            imageToUpload.Save(tempPath, jpegCodec, encoderParams);
                        } else {
                            tempPath = Path.Combine(Path.GetTempPath(), fileNamer.GenerateFileName() + ".png");

                            var pngCodec = GetEncoderInfo("image/png");

                            imageToUpload.Save(tempPath, pngCodec, new EncoderParameters());
                        }

                        var returnUrl = uploadEngine.Upload(tempPath);

                        GrabbieEngine.Instance.UploadNotificationSystem.AddToUploadLog(
                            "Upload Completed Sucessfully: " + returnUrl);
                        GrabbieEngine.Instance.AlertTypes[
                            PropertyService.Get("Grabbie.BasicSettings.AlertType", "System Default")].ShowBalloon(
                                BalloonType.Information, "Upload Completed Sucessfully", returnUrl, 5000);
                        Clipboard.SetText(urlShortner.ShortenUrl(returnUrl));

                        GrabbieEngine.Instance.UploadNotificationSystem.AddToUploadLog("---End of Upload " +
                            DateTime.Now + " ---");
                        GrabbieEngine.Instance.UploadNotificationSystem.UpdateStatus("Upload Completed Sucessfully", 100);
                        GrabbieEngine.Instance.UploadNotificationSystem.SetUploadStatus(UploadStatus.Success);
                    } catch (Exception ex) {
                        GrabbieEngine.Instance.UploadNotificationSystem.AddToUploadLog(ex.ToString());
                        GrabbieEngine.Instance.UploadNotificationSystem.AddToUploadLog("Upload Failed: " + ex.Message);
                        GrabbieEngine.Instance.AlertTypes[
                            PropertyService.Get("Grabbie.BasicSettings.AlertType", "System Default")].ShowBalloon(
                                BalloonType.Error, "Upload Failed", ex.Message, 5000);
                        GrabbieEngine.Instance.UploadNotificationSystem.AddToUploadLog("---End of Upload " +
                            DateTime.Now + " ---");
                        GrabbieEngine.Instance.UploadNotificationSystem.UpdateStatus("Upload Failed: " + ex.Message, 100);
                        GrabbieEngine.Instance.UploadNotificationSystem.SetUploadStatus(UploadStatus.Error);
                    }
                    s_InUse = false;
                });
                thred.SetApartmentState(ApartmentState.STA);
                thred.Start();
            }
        }
    }
}