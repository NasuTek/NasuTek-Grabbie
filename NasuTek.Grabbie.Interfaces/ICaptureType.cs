﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     Used for Capture Types, this provides ways to capture sections of the screen
    ///     for uploading. ICaptureType go into /Grabbie/CaptureServices in the
    ///     Addin Description File.
    /// </summary>
    public interface ICaptureType {
        /// <summary>
        ///     The Hotkey used for activating this Capture Type
        /// </summary>
        SystemHotkey Hotkey { get; }
    }
}