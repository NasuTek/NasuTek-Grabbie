﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     Type of Balloon Message being sent to the IAlertType
    /// </summary>
    public enum BalloonType {
        /// <summary>
        ///     Informational Message
        /// </summary>
        Information,

        /// <summary>
        ///     Error Message
        /// </summary>
        Error,

        /// <summary>
        ///     Exclamation Message
        /// </summary>
        Exclamation
    }

    /// <summary>
    ///     Used for sending alerts to a Alert System. Not the same as IUploadNotificationSystem.
    ///     IAlertType go into /Grabbie/AlertTypes in the Addin Description File.
    /// </summary>
    public interface IAlertType {
        /// <summary>
        ///     Shows a Alert to the IAlertType
        /// </summary>
        /// <param name="baloonType">Type of Balloon Message being sent to the IAlertType.</param>
        /// <param name="title">Message Title.</param>
        /// <param name="message">Message.</param>
        /// <param name="timeout">Time when the alert will automatically close.</param>
        void ShowBalloon(BalloonType baloonType, string title, string message, int timeout);
    }
}