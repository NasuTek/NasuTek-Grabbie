#region Using Directives
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.Interfaces.GrabLocationServices {
    /// <summary>
    ///     Used for returning Rectangle Data.
    /// </summary>
    public class RectHelper {
        /// <summary>
        ///     Gets a rectangle from the Active Window
        /// </summary>
        /// <returns>A Rectangle Object from the Active Window's Geometry.</returns>
        public static Rectangle GetRectangleForActiveWindow() {
            var rect = WinApi.GetRectData();
            var foreWindow = WinApi.GetForegroundWindow();
            var forePlacement = new WinApi.WINDOWPLACEMENT();
            forePlacement.length = Marshal.SizeOf(forePlacement);
            WinApi.GetWindowPlacement(foreWindow, ref forePlacement);

            return GetRectangle(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        /// <summary>
        ///     Return a Rectangle from WinAPI Rectangle
        /// </summary>
        /// <param name="x">x param</param>
        /// <param name="y">y param</param>
        /// <param name="width">width param</param>
        /// <param name="height">height param</param>
        /// <returns></returns>
        public static Rectangle GetRectangle(int x, int y, int width, int height) {
            if (width < 0) {
                x += width;
                width = -width;
            }
            if (height < 0) {
                y += height;
                height = -height;
            }
            return new Rectangle(x, y, width, height);
        }
    }
}