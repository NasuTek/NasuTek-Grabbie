#region Using Directives
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

#endregion

namespace NasuTek.Grabbie.Interfaces.GrabLocationServices {
    /// <summary>
    ///     Screen Bound Services
    /// </summary>
    public class ScreenBounds {
        /// <summary>
        ///     Returns the Screen Bounds as a Rectangle Object
        /// </summary>
        /// <returns>Screen Bounds as a Rectangle Object</returns>
        public static Rectangle GetScreenBounds() {
            var point = new Point(0, 0);
            var point2 = new Point(0, 0);
            foreach (var screen in Screen.AllScreens) {
                if (screen.Bounds.X < point.X)
                    point.X = screen.Bounds.X;
                if (screen.Bounds.Y < point.Y)
                    point.Y = screen.Bounds.Y;
                if ((screen.Bounds.X + screen.Bounds.Width) > point2.X)
                    point2.X = screen.Bounds.X + screen.Bounds.Width;
                if ((screen.Bounds.Y + screen.Bounds.Height) > point2.Y)
                    point2.Y = screen.Bounds.Y + screen.Bounds.Height;
            }
            return new Rectangle(point.X, point.Y, point2.X + Math.Abs(point.X), point2.Y + Math.Abs(point.Y));
        }
    }
}