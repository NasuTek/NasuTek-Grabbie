﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     The Grabbie Engine used by Grabbie Services
    /// </summary>
    public class GrabbieEngine {
        /// <summary>
        ///     Constructor
        /// </summary>
        public GrabbieEngine() {
            if (Instance == null) {
                CaptureTypes = new List<ICaptureType>();
                FileNameTypes = new Dictionary<string, IFileNameType>();
                UploadTypes = new Dictionary<string, IUploadType>();
                UrlShortnerTypes = new Dictionary<string, IUrlShortnerType>();
                AlertTypes = new Dictionary<string, IAlertType>();
                SettingsPages = new List<SettingsPage>();

                Instance = this;
            } else {
                //TODO: Throw an exception
            }
        }

        /// <summary>
        ///     GrabbieEngine Instance
        /// </summary>
        public static GrabbieEngine Instance { get; private set; }

        /// <summary>
        ///     Capture Types
        /// </summary>
        public List<ICaptureType> CaptureTypes { get; private set; }

        /// <summary>
        ///     File Name Generators
        /// </summary>
        public Dictionary<string, IFileNameType> FileNameTypes { get; private set; }

        /// <summary>
        ///     Grab Uploaders
        /// </summary>
        public Dictionary<string, IUploadType> UploadTypes { get; private set; }

        /// <summary>
        ///     URL Shortners
        /// </summary>
        public Dictionary<string, IUrlShortnerType> UrlShortnerTypes { get; private set; }

        /// <summary>
        ///     Alerters
        /// </summary>
        public Dictionary<string, IAlertType> AlertTypes { get; private set; }

        /// <summary>
        ///     Setting Pages
        /// </summary>
        public List<SettingsPage> SettingsPages { get; private set; }

        /// <summary>
        ///     Upload Notification System
        /// </summary>
        public IUploadNotificationSystem UploadNotificationSystem { get; set; }

        public ICaptureType GetCaptureTypeByHotkeyName(string hotkeyName) {
            return CaptureTypes.FirstOrDefault(v => v.Hotkey.ShortcutName == hotkeyName);
        }
    }
}