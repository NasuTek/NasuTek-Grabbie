﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     Settings Page used to set settings for the specified function or to
    ///     display information. For saving and containing settings we recommend using
    ///     the PropertyService exposed by ICSharpCode.Core as it will put configurations
    ///     in a global configuration file. For more information see the NasuTek AddIns API.
    ///     SettingsPage go into /Grabbie/SettingsPages in the Addin Description File.
    /// </summary>
    public partial class SettingsPage : UserControl {
        /// <summary>
        ///     Constructor
        /// </summary>
        public SettingsPage() {
            InitializeComponent();
        }

        /// <summary>
        ///     The Page Name of the Settings Page. This must be overrided
        /// </summary>
        public virtual string PageName {
            get { return ""; }
        }

        /// <summary>
        ///     Used to read Settings. Does not have to be overrided but if not overrided
        ///     it just does nothing.
        /// </summary>
        public virtual void Open() {}

        /// <summary>
        ///     Used to save Settings.  Does not have to be overrided but if not overrided
        ///     it just does nothing.
        /// </summary>
        public virtual void Save() {}
    }
}