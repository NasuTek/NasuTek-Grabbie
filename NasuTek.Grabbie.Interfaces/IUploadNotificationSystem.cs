﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace NasuTek.Grabbie.Interfaces {
    /// <summary>
    ///     The Upload Status Enum
    /// </summary>
    public enum UploadStatus {
        /// <summary>
        ///     Uploading the Grab
        /// </summary>
        Uploading,

        /// <summary>
        ///     Upload had an error.
        /// </summary>
        Error,

        /// <summary>
        ///     Upload succeeded
        /// </summary>
        Success,
    }

    /// <summary>
    ///     Used by the core application to get Notifications. This isnt the same as IAlertType
    ///     as IAlertType is expandable by plugins as IUploadNotificationSystem is not. Although
    ///     Plug-Ins can use the IUploadNotificationSystem exposed by GrabbieEngine to send
    ///     notifications to the core application.
    /// </summary>
    public interface IUploadNotificationSystem {
        /// <summary>
        ///     Sets the upload status to the core app. Plugins shouldnt use this function as
        ///     the core sets the proper status itself.
        /// </summary>
        /// <param name="status">The status of the upload, from the UploadStatus enum.</param>
        void SetUploadStatus(UploadStatus status);

        /// <summary>
        ///     Sets the Status Message and its Progress. This function is Plugin Safe
        /// </summary>
        /// <param name="statusMessage">The status message.</param>
        /// <param name="process">
        ///     Upload progress, usually this is written to a progressbar
        ///     so usually the valid area is from 0 to 100
        /// </param>
        void UpdateStatus(string statusMessage, int process);

        /// <summary>
        ///     Adds the following text to the Upload Log for that Grabbie Session. This function
        ///     is Plugin Safe.
        /// </summary>
        /// <param name="text">Text to add.</param>
        void AddToUploadLog(string text);
    }
}