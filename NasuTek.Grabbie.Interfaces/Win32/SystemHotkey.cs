#region Licensing and File Data
// Grabbie 3.0
// Copyright 2009-2010 NasuTek-Alliant Enterprises
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion

#region Using Directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

#endregion

namespace NasuTek.Grabbie.Interfaces.Win32 {
    /// <summary>
    ///     Handles a System Hotkey
    /// </summary>
    public class SystemHotkey : Component, IDisposable {
// ReSharper disable InconsistentNaming
        private readonly WinApi.DummyWindowWithEvent m_Window = new WinApi.DummyWindowWithEvent();
        //window for WM_Hotkey Messages

        protected bool isRegistered;
        protected Shortcut m_HotKey = Shortcut.None;

// ReSharper restore InconsistentNaming

        public SystemHotkey(IContainer container) {
            container.Add(this);
            InitializeComponent();
            m_Window.ProcessMessage += MessageEvent;
        }

        public SystemHotkey() {
            InitializeComponent();
            if (!DesignMode) m_Window.ProcessMessage += MessageEvent;
        }

        public string ShortcutName { get; set; }

        public bool IsRegistered {
            get { return isRegistered; }
        }

        [DefaultValue(Shortcut.None)] public Shortcut Shortcut {
            get { return m_HotKey; }
            set {
                if (DesignMode) {
                    m_HotKey = value;
                    return;
                } //Don't register in Designmode
                if ((isRegistered) && (m_HotKey != value)) //Unregister previous registered Hotkey
                {
                    if (UnregisterHotkey()) {
                        Debug.WriteLine("Unreg: OK");
                        isRegistered = false;
                    } else {
                        if (Error != null) Error(this, EventArgs.Empty);
                        Debug.WriteLine("Unreg: ERR");
                    }
                }
                if (value == Shortcut.None) {
                    m_HotKey = value;
                    return;
                }
                if (RegisterHotkey(value)) //Register new Hotkey
                {
                    Debug.WriteLine("Reg: OK");
                    isRegistered = true;
                } else {
                    if (Error != null) Error(this, EventArgs.Empty);
                    Debug.WriteLine("Reg: ERR");
                }
                m_HotKey = value;
            }
        }

        #region IDisposable Members
        public new void Dispose() {
            if (isRegistered) {
                if (UnregisterHotkey())
                    Debug.WriteLine("Unreg: OK");
            }
            Debug.WriteLine("Disposed");
        }
        #endregion

        #region Component Designer generated code
        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {}
        #endregion

        public event EventHandler Pressed;
        public event EventHandler Error;

        protected void MessageEvent(object sender, ref Message m, ref bool handled) {
            //Handle WM_Hotkey event
            if ((m.Msg == (int) WinApi.Msgs.WM_HOTKEY) && (m.WParam == (IntPtr) GetType().GetHashCode())) {
                handled = true;
                Debug.WriteLine("HOTKEY pressed!");
                if (Pressed != null) Pressed(this, EventArgs.Empty);
            }
        }

        protected bool UnregisterHotkey() {
            //unregister hotkey
            return WinApi.UnregisterHotKey(m_Window.Handle, GetType().GetHashCode());
        }

        protected bool RegisterHotkey(Shortcut key) {
            //register hotkey
            var mod = 0;
            var k2 = Keys.None;
            if (((int) key & (int) Keys.Alt) == (int) Keys.Alt) {
                mod += (int) WinApi.Modifiers.MOD_ALT;
                k2 = Keys.Alt;
            }
            if (((int) key & (int) Keys.Shift) == (int) Keys.Shift) {
                mod += (int) WinApi.Modifiers.MOD_SHIFT;
                k2 = Keys.Shift;
            }
            if (((int) key & (int) Keys.Control) == (int) Keys.Control) {
                mod += (int) WinApi.Modifiers.MOD_CONTROL;
                k2 = Keys.Control;
            }

            Debug.Write(mod + " ");
            Debug.WriteLine((((int) key) - ((int) k2)).ToString(CultureInfo.InvariantCulture));

            return WinApi.RegisterHotKey(m_Window.Handle, GetType().GetHashCode(), mod,
                ((int) key) - ((int) k2));
        }
    }
}