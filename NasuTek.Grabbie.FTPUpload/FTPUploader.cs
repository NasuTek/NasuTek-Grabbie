﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.FTPUpload {
    public class FtpUploader : IUploadType {
        private Exception m_Holder;
        private bool m_Uploading;

        public string Upload(string imagePath) {
            m_Holder = null;

            GrabbieEngine.Instance.UploadNotificationSystem.UpdateStatus("Uploading", 0);

            var host = PropertyService.Get("Grabbie.FtpUploader.Host");
            var ftpPath = PropertyService.Get("Grabbie.FtpUploader.FtpPath");
            var userName = PropertyService.Get("Grabbie.FtpUploader.UserName");
            var password = PropertyService.Get("Grabbie.FtpUploader.Password");
            var httpPath = PropertyService.Get("Grabbie.FtpUploader.HttpPath");

            var webClient = new WebClient {
                Credentials = new NetworkCredential(userName, password)
            };

            webClient.UploadProgressChanged += webClient_UploadProgressChanged;
            webClient.UploadDataCompleted += webClient_UploadDataCompleted;

            var stream = File.OpenRead(imagePath);
            var buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            stream.Close();

            m_Uploading = true;

            webClient.UploadDataAsync(new Uri("ftp://" + host + ftpPath + Path.GetFileName(imagePath)), buffer);

            while (m_Uploading) Thread.Sleep(500);

            if (m_Holder != null)
                throw new WebException("Error during upload: " + m_Holder.Message, m_Holder);

            return httpPath + Path.GetFileName(imagePath);
        }

        private void webClient_UploadDataCompleted(object sender, UploadDataCompletedEventArgs e) {
            if (e.Error != null)
                m_Holder = e.Error;
            m_Uploading = false;
        }

        private void webClient_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e) {
            GrabbieEngine.Instance.UploadNotificationSystem.UpdateStatus(
                String.Format("Uploading {0}KB of {1}KB", e.BytesSent/1024f, e.TotalBytesToSend/1024f),
                e.ProgressPercentage);
        }
    }
}