﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.FTPUpload {
    public partial class FtpSettings : SettingsPage {
        public FtpSettings() {
            InitializeComponent();
        }

        public override string PageName {
            get { return "FTP Settings"; }
        }

        public override void Open() {
            textBox1.Text = PropertyService.Get("Grabbie.FtpUploader.Host");
            textBox2.Text = PropertyService.Get("Grabbie.FtpUploader.FtpPath");
            textBox3.Text = PropertyService.Get("Grabbie.FtpUploader.UserName");
            textBox4.Text = PropertyService.Get("Grabbie.FtpUploader.Password");
            textBox5.Text = PropertyService.Get("Grabbie.FtpUploader.HttpPath");
        }

        public override void Save() {
            PropertyService.Set("Grabbie.FtpUploader.Host", textBox1.Text);
            PropertyService.Set("Grabbie.FtpUploader.FtpPath", textBox2.Text);
            PropertyService.Set("Grabbie.FtpUploader.UserName", textBox3.Text);
            PropertyService.Set("Grabbie.FtpUploader.Password", textBox4.Text);
            PropertyService.Set("Grabbie.FtpUploader.HttpPath", textBox5.Text);
        }
    }
}