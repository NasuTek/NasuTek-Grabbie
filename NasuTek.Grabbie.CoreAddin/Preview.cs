﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

#endregion

namespace NasuTek.Grabbie.CoreAddin {
    public partial class Preview : Form {
        public Preview(Image img) {
            InitializeComponent();
            pictureBox1.Image = img;
        }
    }
}