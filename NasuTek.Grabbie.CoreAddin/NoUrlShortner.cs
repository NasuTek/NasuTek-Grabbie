﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin {
    public class NoUrlShortner : IUrlShortnerType {
        public string ShortenUrl(string fullUrl) {
            return fullUrl;
        }
    }
}