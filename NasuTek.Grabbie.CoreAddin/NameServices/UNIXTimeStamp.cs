﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin.NameServices {
    public class UnixTimeStamp : IFileNameType {
        public string GenerateFileName() {
            return
                ((int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds).ToString(CultureInfo.InvariantCulture);
        }
    }
}