﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin.NameServices {
    public class CurrentTime : IFileNameType {
        public string GenerateFileName() {
            return DateTime.Now.ToString("HHmmssMMddyyyy");
        }
    }
}