﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin.NameServices {
    public class GuidFileName : IFileNameType {
        public string GenerateFileName() {
            return Guid.NewGuid().ToString();
        }
    }
}