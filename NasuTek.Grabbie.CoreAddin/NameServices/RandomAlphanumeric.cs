﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin.NameServices {
    public class RandomAlphanumeric : IFileNameType {
        public string GenerateFileName() {
            return GetRandomString(new Random(), 8);
        }

        public string GetRandomString(Random rnd, int length) {
            const string charPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            var rs = new StringBuilder();

            while (length-- != 0)
                rs.Append(charPool[(int) (rnd.NextDouble()*charPool.Length)]);

            return rs.ToString();
        }
    }
}