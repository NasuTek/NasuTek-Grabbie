﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NasuTek.Grabbie.Interfaces;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.CoreAddin.SelectionGrab {
    public class SelectionGrab : ICaptureType {
        public SelectionGrab() {
            Hotkey = new SystemHotkey {
                ShortcutName = "Selection Grab",
                Shortcut = Shortcut.AltF11
            };
            Hotkey.Pressed += Hotkey_Pressed;
        }

        public SystemHotkey Hotkey { get; private set; }

        private void Hotkey_Pressed(object sender, EventArgs e) {
            new GrabSection().Show();
        }
    }
}