#region Using Directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces.GrabLocationServices;
using NasuTek.Grabbie.Interfaces.ImagingServices;
using NasuTek.Grabbie.Interfaces.UploadServices;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.CoreAddin.SelectionGrab {
    public class GrabSection : Form {
        private static IntPtr s_HookId = IntPtr.Zero;
        public static bool IsInstanceRunning = false;
        private readonly IContainer components;
        public bool EnterKeyDown;
        private int m_HighX;
        private int m_HighY;
        Rectangle m_ImageRect;
        private Point m_CursorPos = new Point(0, 0);
        private WinApi.LowLevelKeyboardProc m_Proc;
        bool m_MouseDown;
        Point m_MouseDownLocation;

        public GrabSection() {
            InitializeComponent();
            IsInstanceRunning = true;
        }

        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
                components.Dispose();
            Dispose(disposing);
        }

        private void GrabbieMain_FormClosed(object sender, FormClosedEventArgs e) {
            IsInstanceRunning = false;
            try {
                WinApi.UnhookWindowsHookEx(s_HookId);
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_KeyDown(object sender, KeyEventArgs e) {
            try {
                if (e.KeyCode == Keys.Escape)
                    Close();
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_MouseDown(object sender, MouseEventArgs e) {
            try {
                if (e.Button == MouseButtons.Left) {
                    m_MouseDown = true;
                    m_MouseDownLocation = e.Location;
                    GrabbieMain_MouseMove(this, e);
                }
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_MouseMove(object sender, MouseEventArgs e) {
            try {
                m_CursorPos.X = e.X;
                m_CursorPos.Y = e.Y;
                if (m_MouseDown) {
                    var x = m_MouseDownLocation.X;
                    var y = m_MouseDownLocation.Y;
                    var width = e.X - m_MouseDownLocation.X;
                    var height = e.Y - m_MouseDownLocation.Y;
                    m_ImageRect = RectHelper.GetRectangle(x, y, width, height);
                }
                Invalidate();
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_MouseUp(object sender, MouseEventArgs e) {
            try {
                if (m_MouseDown && (e.Button == MouseButtons.Left)) {
                    m_MouseDown = false;
                    m_CursorPos.X = 0;
                    m_CursorPos.Y = 0;
                    Close();
                    PostScreenSelection(m_ImageRect);
                }
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_Paint(object sender, PaintEventArgs e) {
            try {
                var graphics = e.Graphics;
                var s = "X: " + m_CursorPos.X.ToString(CultureInfo.InvariantCulture) + "\nY: " + m_CursorPos.Y.ToString(CultureInfo.InvariantCulture);
                var height = 0x15;
                if (m_MouseDown) {
                    Brush brush = new SolidBrush(Color.FromArgb(0xff, Color.Gray));
                    var pen = new Pen(Color.FromArgb(0xff, Color.White), 2f);
                    graphics.FillRectangle(brush, m_ImageRect);
                    graphics.DrawRectangle(pen, m_ImageRect);
                    object obj2 = s;
                    s = string.Concat(new[] {obj2, "\nW: ", m_ImageRect.Width, "\nH: ", m_ImageRect.Height});
                    height = 0x2e;
                }
                if ((m_CursorPos.X >= 0) || (m_CursorPos.Y >= 0)) {
                    var font = new Font(FontFamily.GenericSansSerif, 7f);
                    Brush brush2 = new SolidBrush(Color.FromArgb(0xff, Color.White));
                    if (((m_HighX - m_CursorPos.X) < 60) && ((m_HighY - m_CursorPos.Y) < 60)) {
                        graphics.FillRectangle(brush2,
                            new Rectangle(m_CursorPos.X - 50, m_CursorPos.Y - 40, 0x29, height));
                        graphics.DrawString(s, font, new Pen(Color.Black).Brush, (m_CursorPos.X - 50),
                            (m_CursorPos.Y - 40));
                    } else if ((m_HighX - m_CursorPos.X) < 50) {
                        graphics.FillRectangle(brush2,
                            new Rectangle(m_CursorPos.X - 50, m_CursorPos.Y + 15, 0x29, height));
                        graphics.DrawString(s, font, new Pen(Color.Black).Brush, (m_CursorPos.X - 50),
                            (m_CursorPos.Y + 15));
                    } else if ((m_HighY - m_CursorPos.Y) < 50) {
                        graphics.FillRectangle(brush2,
                            new Rectangle(m_CursorPos.X + 15, m_CursorPos.Y - 40, 0x29, height));
                        graphics.DrawString(s, font, new Pen(Color.Black).Brush, (m_CursorPos.X + 15),
                            (m_CursorPos.Y - 40));
                    } else {
                        graphics.FillRectangle(brush2,
                            new Rectangle(m_CursorPos.X + 5, m_CursorPos.Y + 5, 0x29, height));
                        graphics.DrawString(s, font, new Pen(Color.Black).Brush, (m_CursorPos.X + 5),
                            (m_CursorPos.Y + 5));
                    }
                }
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private void GrabbieMain_Shown(object sender, EventArgs e) {
            try {
                var r = new Rectangle();
                r = Screen.AllScreens.Aggregate(r, (current, s) => Rectangle.Union(current, s.Bounds));
                Bounds = new Rectangle(r.Left, r.Top, r.Width, r.Height);
                m_HighX = Bounds.Width;
                m_HighY = Bounds.Height;
                m_ImageRect = Bounds;
                m_Proc = HookCallback;
                s_HookId = SetHook(m_Proc);
                Activate();
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam) {
            try {
                if (((nCode >= 0) && (wParam == ((IntPtr) 0x100))) || (wParam == ((IntPtr) 260))) {
                    var num = Marshal.ReadInt32(lParam);
                    if (0x1b == num)
                        Close();
                    else {
                        BringToFront();
                        Activate();
                    }
                }
                return WinApi.CallNextHookEx(s_HookId, nCode, wParam, lParam);
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
                return IntPtr.Zero;
            }
        }

        private void InitializeComponent() {
            SuspendLayout();
            // 
            // GrabbieMain
            // 
            ClientSize = new Size(116, 5);
            Cursor = Cursors.Cross;
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;
            Name = "GrabbieMain";
            Opacity = 0.35D;
            ShowInTaskbar = false;
            Text = "GrabbieMain";
            TopMost = true;
            FormClosed += GrabbieMain_FormClosed;
            Shown += GrabbieMain_Shown;
            Paint += GrabbieMain_Paint;
            KeyDown += GrabbieMain_KeyDown;
            MouseDown += GrabbieMain_MouseDown;
            MouseMove += GrabbieMain_MouseMove;
            MouseUp += GrabbieMain_MouseUp;
            ResumeLayout(false);
        }

        private void PostScreenSelection(Rectangle rect) {
            try {
                var grab = GrabbieImagingServices.Grab(rect);
                if (!PropertyService.Get("Grabbie.BasicSettings.PreviewBeforeUpload", false))
                    GrabbieUploaderService.UploadCapture(grab);
                else {
                    if (new Preview(grab).ShowDialog() == DialogResult.Yes)
                        GrabbieUploaderService.UploadCapture(grab);
                }
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
            }
        }

        private static IntPtr SetHook(WinApi.LowLevelKeyboardProc proc) {
            IntPtr zero;
            try {
                using (var process = Process.GetCurrentProcess())
                using (var module = process.MainModule)
                    zero = WinApi.SetWindowsHookEx(13, proc, WinApi.GetModuleHandle(module.ModuleName), 0);
            } catch (Exception ex) {
                //TODO: Add are lovely Except Handler Here
                zero = IntPtr.Zero;
            }
            return zero;
        }
    }
}