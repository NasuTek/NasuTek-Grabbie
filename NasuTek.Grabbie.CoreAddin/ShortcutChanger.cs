﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.CoreAddin {
    public partial class ShortcutChanger : UserControl {
        private readonly SystemHotkey m_Hotkey;

        public ShortcutChanger(SystemHotkey hotkey) {
            InitializeComponent();
            m_Hotkey = hotkey;
            Dock = DockStyle.Top;
            textBox1.Text = ConvertfromHotkey(hotkey.Shortcut.ToString());
            label1.Text = HotkeyName;
        }

        public string HotkeyName {
            get { return m_Hotkey.ShortcutName; }
        }

        public string HotkeyText {
            get { return ConverttoHotkey(textBox1.Text); }
        }

        public void ApplyHotkey() {
            m_Hotkey.Shortcut = (Shortcut) Enum.Parse(typeof (Shortcut), HotkeyText);
            PropertyService.Set("Grabbie.Hotkey." + HotkeyName.Replace(" ", ""), m_Hotkey.Shortcut);
        }

        private bool IsNumeric(string numericstring) {
            var returnshit = Regex.IsMatch(numericstring, "\\d+");
            return returnshit;
        }

        private bool Validatehotkey(string hotkey) {
            var isvalid = false;
            try {
                hotkey = Regex.Replace(hotkey, " ", String.Empty);
                var parts = hotkey.Split('+');
                var part1 = parts[0];
                var part2 = parts[1];
                var part3 = parts[parts.Length - 1];
                if (part1.Substring(0, 1) == "F" && part1.Length > 1) isvalid = true;
                if (part1 == "Insert") isvalid = true;
                if (part1 == "Delete") isvalid = true;
                if (part1 == "Ctrl") {
                    if (part2 == "Shift") {
                        if (IsNumeric(part3))
                            isvalid = true;
                        if (Regex.IsMatch(part3, "^[A-Za-z]$"))
                            isvalid = true;
                        if (part3.Substring(0, 1) == "F" && part3.Length > 1)
                            isvalid = true;
                    } else {
                        if (IsNumeric(part2)) isvalid = true;
                        if (part2 == "Delete") isvalid = true;
                        if (part2 == "Insert") isvalid = true;
                        if (Regex.IsMatch(part2, "^[A-Za-z]$")) isvalid = true;
                        if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                            isvalid = true;
                    }
                }
                if (part1 == "Alt") {
                    if (IsNumeric(part2)) isvalid = true;
                    if (part2 == "Down") isvalid = true;
                    if (part2 == "Up") isvalid = true;
                    if (part2 == "Left") isvalid = true;
                    if (part2 == "Right") isvalid = true;
                    if (part2 == "Back") isvalid = true;
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1) isvalid = true;
                }
                if (part1 == "Shift") {
                    if (part2 == "Insert") isvalid = true;
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        isvalid = true;
                }
            } catch {}
            return isvalid;
        }

        private string ConverttoHotkey(string hotkey) {
            var realhotkey = "None";
            try {
                hotkey = Regex.Replace(hotkey, " ", String.Empty);
                var parts = hotkey.Split('+');
                var part1 = parts[0];
                var part2 = parts[1];
                var part3 = parts[parts.Length - 1];
                if (part1.Substring(0, 1) == "F" && part1.Length > 1)
                    realhotkey = part1;
                if (part1 == "Insert")
                    realhotkey = part1;
                if (part1 == "Delete")
                    realhotkey = part1;
                if (part1 == "Ctrl") {
                    if (part2 == "Shift") {
                        if (IsNumeric(part3))
                            realhotkey = part1 + part2 + part3;
                        if (Regex.IsMatch(part3, "^[A-Za-z]$"))
                            realhotkey = part1 + part2 + part3;
                        if (part3.Substring(0, 1) == "F" && part3.Length > 1)
                            realhotkey = part1 + part2 + part3;
                    } else {
                        if (IsNumeric(part2))
                            realhotkey = part1 + part2;
                        if (part2 == "Delete")
                            realhotkey = part1 + "Del";
                        if (part2 == "Insert")
                            realhotkey = part1 + "Ins";
                        if (Regex.IsMatch(part2, "^[A-Za-z]$"))
                            realhotkey = part1 + part2;
                        if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                            realhotkey = part1 + part2;
                    }
                }
                if (part1 == "Alt") {
                    if (IsNumeric(part2))
                        realhotkey = part1 + part2;
                    if (part2 == "Down")
                        realhotkey = part1 + "DownArrow";
                    if (part2 == "Up")
                        realhotkey = part1 + "UpArrow";
                    if (part2 == "Left")
                        realhotkey = part1 + "LeftArrow";
                    if (part2 == "Right")
                        realhotkey = part1 + "RightArrow";
                    if (part2 == "Back")
                        realhotkey = part1 + "Bksp";
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        realhotkey = part1 + part2;
                }
                if (part1 == "Shift") {
                    if (part2 == "Insert")
                        realhotkey = part1 + "Ins";
                    if (part2.Substring(0, 1) == "F" && part2.Length > 1)
                        realhotkey = part1 + part2;
                }
            } catch {}
            return realhotkey;
        }

        internal string ConvertfromHotkey(string hotkey) {
            var realhotkey = "None";

            if (hotkey.Substring(0, 4) == "Ctrl") {
                if (hotkey.Length > 9 && hotkey.Substring(0, 9) == "CtrlShift") {
                    if (IsNumeric(hotkey.Substring(9, 1)))
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, 1);
                    if (Regex.IsMatch(hotkey.Substring(9, 1), "^[A-Za-z]$"))
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, 1);
                    if (hotkey.Substring(9, 1) == "F")
                        realhotkey = "Ctrl + Shift + " + hotkey.Substring(9, hotkey.Length - 9);
                } else {
                    if (IsNumeric(hotkey.Substring(4, 1)))
                        realhotkey = "Ctrl + " + hotkey.Substring(4, 1);
                    if (Regex.IsMatch(hotkey.Substring(4, 1), "^[A-Za-z]$"))
                        realhotkey = "Ctrl + " + hotkey.Substring(4, 1);
                    if (hotkey.Substring(0, 4)[1].ToString(CultureInfo.InvariantCulture) == "Del")
                        realhotkey = "Ctrl + Delete";
                    if (hotkey.Substring(0, 4)[1].ToString(CultureInfo.InvariantCulture) == "Ins")
                        realhotkey = "Ctrl + Insert";
                    if (hotkey.Substring(4, 1) == "F")
                        realhotkey = "Ctrl + " + hotkey.Substring(4, hotkey.Length - 4);
                }
            }
            if (hotkey.Substring(0, 3) == "Alt") {
                if (IsNumeric(hotkey.Substring(3, 1))) realhotkey = "Alt + " + hotkey.Substring(3, 1);
                if (hotkey.Substring(3, hotkey.Length - 3) == "DownArrow")
                    realhotkey = "Alt + Down";
                if (hotkey.Substring(3, hotkey.Length - 3) == "UpArrow")
                    realhotkey = "Alt + Up";
                if (hotkey.Substring(3, hotkey.Length - 3) == "LeftArrow")
                    realhotkey = "Alt + Left";
                if (hotkey.Substring(3, hotkey.Length - 3) == "RightArrow")
                    realhotkey = "Alt + Right";
                if (hotkey.Substring(3, hotkey.Length - 3) == "Bksp")
                    realhotkey = "Alt + Back";
                if (hotkey.Substring(3, 1) == "F")
                    realhotkey = "Alt + " + hotkey.Substring(3, hotkey.Length - 3);
            }
            if (hotkey.Length > 4 && hotkey.Substring(0, 5) == "Shift") {
                if (hotkey.Substring(5, hotkey.Length - 5) == "Ins")
                    realhotkey = "Shift + Insert";
                if (hotkey.Substring(5, 1) == "F")
                    realhotkey = "Shift + " + hotkey.Substring(5, hotkey.Length - 5);
            }
            if (hotkey.Substring(0, 1) == "F" && hotkey.Length > 1)
                realhotkey = hotkey;

            return realhotkey;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e) {
            e.Handled = true;
            string keyhook;
            if (e.KeyCode == Keys.Back)
                textBox1.Text = "Alt + Back";
            if (e.KeyCode.ToString() == "Insert")
                textBox1.Text = "Insert";
            if (e.KeyCode.ToString() == "Delete")
                textBox1.Text = "Delete";
            /*if (e.KeyCode.ToString().Substring(0, 1) == "F" && e.KeyCode.ToString().Length > 1 && !e.Shift && !e.Control && !e.Alt) {
                keyhook = e.KeyCode.ToString ();
                if (Validatehotkey (keyhook) == false) {
                    keyhook = "None";
                }
                textBox7.Text = keyhook;
            }*/
            if (e.Control) {
                keyhook = "Ctrl";
                if (e.Shift) {
                    keyhook = "Ctrl + Shift";
                    if (e.KeyCode.ToString() != "ShiftKey") {
                        var ichar = e.KeyCode.ToString();

                        if (ichar.Contains("D") && ichar.Length == 2)
                            ichar = Regex.Replace(ichar, "D", String.Empty);
                        keyhook = "Ctrl + Shift + " + ichar;
                        if (Validatehotkey(keyhook) == false)
                            keyhook = "Ctrl + Shift";
                    }
                }
                if (e.KeyCode.ToString() != "ControlKey" && !e.Shift) {
                    var ichar = e.KeyCode.ToString();

                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Ctrl + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Ctrl";
                }
                textBox1.Text = keyhook;
            }

            if (e.Alt) {
                keyhook = "Alt";
                if (e.KeyCode.ToString() != "Menu") {
                    var ichar = e.KeyCode.ToString();
                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Alt + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Alt";
                }
                textBox1.Text = keyhook;
            }
            if (e.Shift && !e.Control) {
                keyhook = "Shift";
                if (e.KeyCode.ToString() != "ShiftKey" && !e.Control) {
                    var ichar = e.KeyCode.ToString();

                    if (ichar.Contains("D") && ichar.Length == 2)
                        ichar = Regex.Replace(ichar, "D", String.Empty);
                    keyhook = "Shift + " + ichar;
                    if (Validatehotkey(keyhook) == false)
                        keyhook = "Shift";
                }
                textBox1.Text = keyhook;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = true;
        }
    }
}