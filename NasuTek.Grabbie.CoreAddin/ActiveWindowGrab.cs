﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ICSharpCode.Core;
using NasuTek.Grabbie.Interfaces;
using NasuTek.Grabbie.Interfaces.GrabLocationServices;
using NasuTek.Grabbie.Interfaces.ImagingServices;
using NasuTek.Grabbie.Interfaces.UploadServices;
using NasuTek.Grabbie.Interfaces.Win32;

#endregion

namespace NasuTek.Grabbie.CoreAddin {
    public class ActiveWindowGrab : ICaptureType {
        public ActiveWindowGrab() {
            Hotkey = new SystemHotkey {
                ShortcutName = "Active Window",
                Shortcut = Shortcut.AltF12
            };
            Hotkey.Pressed += Hotkey_Pressed;
        }

        public SystemHotkey Hotkey { get; private set; }

        private void Hotkey_Pressed(object sender, EventArgs e) {
            var grab = GrabbieImagingServices.Grab(RectHelper.GetRectangleForActiveWindow());

            if (!PropertyService.Get("Grabbie.BasicSettings.PreviewBeforeUpload", false))
                GrabbieUploaderService.UploadCapture(grab);
            else {
                if (new Preview(grab).ShowDialog() == DialogResult.Yes)
                    GrabbieUploaderService.UploadCapture(grab);
            }
        }
    }
}