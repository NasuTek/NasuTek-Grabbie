﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NasuTek.Grabbie.Interfaces;

#endregion

namespace NasuTek.Grabbie.CoreAddin {
    public partial class ShortcutSettings : SettingsPage {
        public ShortcutSettings() {
            InitializeComponent();
            foreach (var captureType in GrabbieEngine.Instance.CaptureTypes) {
                var dp = new ShortcutChanger(captureType.Hotkey) {
                    Dock = DockStyle.Top
                };
                panel1.Controls.Add(dp);
                dp.BringToFront();
            }
        }

        public override string PageName {
            get { return "Shortcut Settings"; }
        }

        public override void Save() {
            foreach (var shortcutChanger in panel1.Controls.OfType<ShortcutChanger>()) shortcutChanger.ApplyHotkey();
        }
    }
}